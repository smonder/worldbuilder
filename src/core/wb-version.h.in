/* wb-version.h.in
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

/**
 * WB_MAJOR_VERSION:
 *
 * WorldBuilder major version component (e.g. 1 if %WB_VERSION is 1.2.3)
 */
#define WB_MAJOR_VERSION (@ABI_MAJOR@)

/**
 * WB_MINOR_VERSION:
 *
 * WorldBuilder minor version component (e.g. 2 if %WB_VERSION is 1.2.3)
 */
#define WB_MINOR_VERSION (@ABI_MINOR@)

/**
 * WB_MICRO_VERSION:
 *
 * WorldBuilder micro version component (e.g. 3 if %WB_VERSION is 1.2.3)
 */
#define WB_MICRO_VERSION (@ABI_MICRO@)

/**
 * WB_VERSION
 *
 * WorldBuilder version.
 */
#define WB_VERSION (@VERSION@)

/**
 * WB_VERSION_S:
 *
 * WorldBuilder version, encoded as a string, useful for printing and
 * concatenation.
 */
#define WB_VERSION_S "@VERSION@"

#define WB_ENCODE_VERSION(major,minor,micro) \
        ((major) << 24 | (minor) << 16 | (micro) << 8)

/**
 * WB_VERSION_HEX:
 *
 * WorldBuilder version, encoded as an hexadecimal number, useful for
 * integer comparisons.
 */
#define WB_VERSION_HEX \
        (WB_ENCODE_VERSION (WB_MAJOR_VERSION, WB_MINOR_VERSION, WB_MICRO_VERSION))

/**
 * WB_CHECK_VERSION:
 * @major: required major version
 * @minor: required minor version
 * @micro: required micro version
 *
 * Compile-time version checking. Evaluates to %TRUE if the version
 * of WorldBuilder is greater than the required one.
 */
#define WB_CHECK_VERSION(major,minor,micro)   \
        (WB_MAJOR_VERSION > (major) || \
         (WB_MAJOR_VERSION == (major) && WB_MINOR_VERSION > (minor)) || \
         (WB_MAJOR_VERSION == (major) && WB_MINOR_VERSION == (minor) && \
          WB_MICRO_VERSION >= (micro)))
