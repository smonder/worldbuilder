/* wb-vertex.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>
#include <graphene-gobject.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_VERTEX (wb_vertex_get_type ())

typedef struct _WbVertex WbVertex;

struct _WbVertex {
  graphene_point3d_t  position;
  GdkRGBA             color;
  graphene_point_t    texture_uv;
  gfloat              texture_id;
};

WB_AVAILABLE_IN_ALL
GType      wb_vertex_get_type     (void) G_GNUC_CONST;
WB_AVAILABLE_IN_ALL
WbVertex * wb_vertex_new          (void);
WB_AVAILABLE_IN_ALL
WbVertex * wb_vertex_copy         (WbVertex * self);
WB_AVAILABLE_IN_ALL
void       wb_vertex_free         (WbVertex * self);

WB_AVAILABLE_IN_ALL
void       wb_vertex_setup_layout (const guint node_id);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (WbVertex, wb_vertex_free)

G_END_DECLS
