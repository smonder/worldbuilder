/* wb-vertex.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbVertex"

#include "config.h"

#include <epoxy/gl.h>

#include "wb-vertex.h"

G_DEFINE_BOXED_TYPE (WbVertex, wb_vertex, wb_vertex_copy, wb_vertex_free)

/**
 * wb_vertex_new:
 *
 * Creates a new #WbVertex.
 *
 * Returns: (transfer full): A newly created #WbVertex
 */
WbVertex *
wb_vertex_new (void)
{
  WbVertex *self;

  self = g_slice_new0 (WbVertex);

  self->position = (graphene_point3d_t){ 0.0f, 0.0f, 0.0f };
  self->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  self->texture_uv = (graphene_point_t){ 0.0f, 0.0f };

  self->texture_id = 0.0f;

  return self;
}

/**
 * wb_vertex_copy:
 * @self: a #WbVertex
 *
 * Makes a deep copy of a #WbVertex.
 *
 * Returns: (transfer full): A newly created #WbVertex with the same
 *   contents as @self
 */
WbVertex *
wb_vertex_copy (WbVertex *self)
{
  WbVertex *copy;

  g_return_val_if_fail (self, NULL);

  copy = wb_vertex_new ();

  graphene_point3d_init_from_point (&copy->position, &self->position);
  memcpy (&copy->color, &self->color, sizeof (self->color));
  graphene_point_init_from_point (&copy->texture_uv, &self->texture_uv);
  copy->texture_id = self->texture_id;

  return copy;
}

/**
 * wb_vertex_free:
 * @self: a #WbVertex
 *
 * Frees a #WbVertex allocated using wb_vertex_new()
 * or wb_vertex_copy().
 */
void
wb_vertex_free (WbVertex *self)
{
  g_return_if_fail (self);

  g_slice_free (WbVertex, self);
}


void
wb_vertex_setup_layout (const guint node_id)
{
  g_return_if_fail (node_id != 0);

  epoxy_glEnableVertexArrayAttrib (node_id, 0);
  epoxy_glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof (WbVertex),
                               (gconstpointer)G_STRUCT_OFFSET (WbVertex, position));

  epoxy_glEnableVertexArrayAttrib (node_id, 1);
  epoxy_glVertexAttribPointer (1, 4, GL_FLOAT, GL_FALSE, sizeof (WbVertex),
                               (gconstpointer)G_STRUCT_OFFSET (WbVertex, color));

  epoxy_glEnableVertexArrayAttrib (node_id, 2);
  epoxy_glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, sizeof (WbVertex),
                               (gconstpointer)G_STRUCT_OFFSET (WbVertex, texture_uv));

  epoxy_glEnableVertexArrayAttrib (node_id, 3);
  epoxy_glVertexAttribPointer (3, 1, GL_FLOAT, GL_FALSE, sizeof (WbVertex),
                               (gconstpointer)G_STRUCT_OFFSET (WbVertex, texture_id));
}
