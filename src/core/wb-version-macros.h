/* wb-version-macros.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "wb-version.h"

#ifndef _WB_EXTERN
#define _WB_EXTERN extern
#endif

#ifdef WB_DISABLE_DEPRECATION_WARNINGS
# define WB_DEPRECATED _WB_EXTERN
# define WB_DEPRECATED_FOR(f) _WB_EXTERN
# define WB_UNAVAILABLE(maj,min) _WB_EXTERN
#else
# define WB_DEPRECATED G_DEPRECATED _WB_EXTERN
# define WB_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _WB_EXTERN
# define WB_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _WB_EXTERN
#endif

#define WB_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (WB_MINOR_VERSION == 99)
# define WB_VERSION_CUR_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION + 1, 0))
#elif (WB_MINOR_VERSION % 2)
# define WB_VERSION_CUR_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION, WB_MINOR_VERSION + 1))
#else
# define WB_VERSION_CUR_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION, WB_MINOR_VERSION))
#endif

#if (WB_MINOR_VERSION == 99)
# define WB_VERSION_PREV_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION + 1, 0))
#elif (WB_MINOR_VERSION % 2)
# define WB_VERSION_PREV_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION, WB_MINOR_VERSION - 1))
#else
# define WB_VERSION_PREV_STABLE (G_ENCODE_VERSION (WB_MAJOR_VERSION, WB_MINOR_VERSION - 2))
#endif


#ifndef WB_VERSION_MIN_REQUIRED
# define WB_VERSION_MIN_REQUIRED (WB_VERSION_CUR_STABLE)
#endif

#ifndef WB_VERSION_MAX_ALLOWED
# if WB_VERSION_MIN_REQUIRED > WB_VERSION_PREV_STABLE
#  define WB_VERSION_MAX_ALLOWED (WB_VERSION_MIN_REQUIRED)
# else
#  define WB_VERSION_MAX_ALLOWED (WB_VERSION_CUR_STABLE)
# endif
#endif

#if WB_VERSION_MAX_ALLOWED < WB_VERSION_MIN_REQUIRED
#error "WB_VERSION_MAX_ALLOWED must be >= WB_VERSION_MIN_REQUIRED"
#endif
#if WB_VERSION_MIN_REQUIRED < WB_VERSION_1_0
#error "WB_VERSION_MIN_REQUIRED must be >= WB_VERSION_1_0"
#endif

#define WB_AVAILABLE_IN_ALL                  _WB_EXTERN

#if WB_VERSION_MIN_REQUIRED >= WB_VERSION_1_0
# define WB_DEPRECATED_IN_1_0                WB_DEPRECATED
# define WB_DEPRECATED_IN_1_0_FOR(f)         WB_DEPRECATED_FOR(f)
#else
# define WB_DEPRECATED_IN_1_0                _WB_EXTERN
# define WB_DEPRECATED_IN_1_0_FOR(f)         _WB_EXTERN
#endif

#if WB_VERSION_MAX_ALLOWED < WB_VERSION_1_0
# define WB_AVAILABLE_IN_1_0                 WB_UNAVAILABLE(1, 0)
#else
# define WB_AVAILABLE_IN_1_0                 _WB_EXTERN
#endif
