/* wb-window.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbWindow"

#include "config.h"

#include <glib/gi18n.h>
#include <epoxy/gl.h>

#include "wb-camera.h"
#include "wb-environment.h"
#include "wb-generic-renderer.h"
#include "wb-loader.h"
#include "wb-macros.h"
#include "wb-private.h"
#include "wb-window.h"

struct _WbWindow
{
  AdwApplicationWindow  parent_instance;

  GdkGLContext * gl_context;
  GdkRectangle   gl_viewport;

  WbCamera * camera;
  WbEnvironment * env;

  /* Renderers */
  WbRenderer * generic_renderer;

  /* Template widgets */
  AdwHeaderBar *    header_bar;
  AdwToastOverlay * notifications;
  GtkOverlay *      overlays;
  GtkGLArea *       viewport;

  /* For update signals */
  gint64  first_frame_time;
  guint   tick;

  /* Queued source to save window size/etc */
  guint queued_window_save;

  /* GTK Event Controllers for viewport */
  GtkGestureClick *          click_gesture;
  GtkGestureDrag *           drag_gesture;
  GtkEventControllerScroll * scroll_event;
  GtkEventControllerKey *    key_event;
};

G_DEFINE_FINAL_TYPE (WbWindow, wb_window, ADW_TYPE_APPLICATION_WINDOW)

static GSettings * settings;

#ifdef ENABLE_TRACING
static void
opengl_message_callback (unsigned int  source,
                         unsigned int  type,
                         unsigned int  id,
                         unsigned int  severity,
                         int           length,
                         const char   *message,
                         const void   *user_data)
{
  WbWindow *self = (WbWindow *)user_data;

  g_assert (WB_IS_WINDOW (self));

  switch (severity)
    {
		case GL_DEBUG_SEVERITY_HIGH:
      g_error ("SOURCE: %d - TYPE: %d :: %s", source, type, message);
      return;

		case GL_DEBUG_SEVERITY_MEDIUM:
      g_critical ("SOURCE: %d - TYPE: %d :: %s", source, type, message);
      return;

		case GL_DEBUG_SEVERITY_LOW:
      g_warning ("SOURCE: %d - TYPE: %d :: %s", source, type, message);
      return;

		case GL_DEBUG_SEVERITY_NOTIFICATION:
      g_message ("SOURCE: %d - TYPE: %d :: %s", source, type, message);
      return;

    default:
		  g_critical ("Unknown severity level!");
      return;
		}
}
#endif


static gboolean
on_viewport_render_cb (WbWindow     *self,
                       GdkGLContext *context,
                       GtkGLArea    *viewport)
{
  g_assert (WB_IS_WINDOW (self));
  g_assert (GDK_IS_GL_CONTEXT (context));
  g_assert (GTK_IS_GL_AREA (viewport));

  if (gtk_gl_area_get_error (viewport) != NULL)
    return FALSE;

  epoxy_glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /* Always pass NULL as shader for the generic renderer */
  wb_renderer_render_all (self->generic_renderer, NULL, self->env, self->camera);

  wb_environment_render (self->env, self->camera);

  epoxy_glFlush ();

  return TRUE;
}

static GdkGLContext*
on_viewport_create_context_cb (WbWindow  *self,
                               GtkGLArea *viewport)
{
  g_autoptr (GdkGLContext) gl_context = NULL;
  GdkSurface * surface;
  GError * error = NULL;

  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GL_AREA (viewport));

  surface = gtk_native_get_surface (gtk_widget_get_native (GTK_WIDGET (viewport)));

  gl_context = gdk_surface_create_gl_context (surface, &error);
  if (!gl_context)
    {
      g_set_error (&error, G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   _("Failed to create GL Context for surface %p. WorldBuilder can't continue"), surface);
      gtk_gl_area_set_error (viewport, error);
      g_clear_error (&error);
      return NULL;
    }

  gdk_gl_context_set_allowed_apis (gl_context, GDK_GL_API_GL);
  gdk_gl_context_set_forward_compatible (gl_context, FALSE);
  gdk_gl_context_set_required_version (gl_context,
                                       OPENGL_VERSION_MAJOR_REQUIRED,
                                       OPENGL_VERSION_MINOR_REQUIRED);

#ifdef ENABLE_TRACING
  gdk_gl_context_set_debug_enabled (gl_context, TRUE);
#endif

  gdk_gl_context_realize (gl_context, &error);
  if (error != NULL)
    {
      gtk_gl_area_set_error (viewport, error);
      g_clear_error (&error);
      return NULL;
    }

  return g_steal_pointer (&gl_context);
}

static void
on_viewport_resize_cb (WbWindow  *self,
                       gint       width,
                       gint       height,
                       GtkGLArea *viewport)
{
  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GL_AREA (viewport));

  self->gl_viewport = (GdkRectangle){0, 0, width, height};

  wb_camera_set_viewport (self->camera, &self->gl_viewport);
  /* TODO: make world_renderer update the viewport */

  glViewport (0, 0, (gfloat)width, (gfloat)height);
}

static void
on_viewport_realize_cb (WbWindow     *self,
                        GtkGLArea    *viewport)
{
  gint context_flags = 0;
  
  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GL_AREA (viewport));
  /* We need to make the context current if we want to call GL API */
  gtk_gl_area_make_current (viewport);

  self->gl_context = gtk_gl_area_get_context (viewport);

  /* If there were errors during the initialization or when trying to make
   * the context current, this function will return a GError for you to catch.
   */
  if (gtk_gl_area_get_error (viewport) != NULL)
    return;

  epoxy_glEnable (GL_DEPTH_TEST);
  epoxy_glDepthFunc (GL_LEQUAL);
  epoxy_glDepthRange (0.0f, 1.0f);

  epoxy_glEnable (GL_BLEND);
  epoxy_glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  epoxy_glEnable (GL_CULL_FACE);
  epoxy_glFrontFace (GL_CCW);
  epoxy_glCullFace (GL_BACK);

#ifdef ENABLE_TRACING
  epoxy_glGetIntegerv (GL_CONTEXT_FLAGS, &context_flags);
  if (context_flags & GL_CONTEXT_FLAG_DEBUG_BIT)
    {
      g_message ("Running OpenGL %d.%d in DEBUG mode",
                 OPENGL_VERSION_MAJOR_REQUIRED,
                 OPENGL_VERSION_MINOR_REQUIRED);
      epoxy_glEnable (GL_DEBUG_OUTPUT);
      epoxy_glEnable (GL_DEBUG_OUTPUT_SYNCHRONOUS);
      epoxy_glDebugMessageCallback (opengl_message_callback, self); /* callback, user_data */
      epoxy_glDebugMessageControl (GL_DONT_CARE,                    /* source */
                                   GL_DONT_CARE,                    /* type */
                                   GL_DEBUG_SEVERITY_NOTIFICATION,  /* severity */
                                   0, NULL, GL_TRUE);
    }
#endif

  /* TODO: prepare renderers here. */
  self->camera = wb_camera_new ();

  self->env = wb_environment_new ();
  wb_environment_prepare (self->env);
  wb_environment_set_horizon_color (self->env,
                                    &(GdkRGBA){ 0.3f, 0.3f, 0.3f, 1.0f });

  self->generic_renderer = wb_generic_renderer_new ();

  /* Loading Assets */
  _wb_load_game_assets_for_renderer (self->generic_renderer);


  /* Testing GUI */
  GtkWidget * diffuse_scale;

  diffuse_scale = gtk_spin_button_new_with_range (0.0f, 1.0f, 0.01f);

  g_object_bind_property (wb_environment_get_sun (self->env), "diffuse",
                          diffuse_scale, "value",
                          (G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL));

  gtk_widget_set_valign (diffuse_scale, GTK_ALIGN_END);
  gtk_widget_set_halign (diffuse_scale, GTK_ALIGN_CENTER);

  wb_window_add_overlay (self, diffuse_scale);
}

static void
on_viewport_unrealize_cb (WbWindow     *self,
                          GtkGLArea    *viewport)
{
  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GL_AREA (viewport));

  if (gtk_gl_area_get_error (viewport) != NULL)
    return;

  /* TODO: clear the renderers here. */
  g_clear_object (&self->camera);
  g_clear_object (&self->env);

  g_clear_object (&self->generic_renderer);
}

static gboolean
on_viewport_update_cb (GtkWidget     *widget,
                       GdkFrameClock *frame_clock,
                       gpointer       user_data)
{
  WbWindow * self = user_data;
  GtkGLArea * viewport = (GtkGLArea *)widget;

  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GL_AREA (viewport));


  if (self->first_frame_time == 0)
    {
      /* No need for changes on first frame */
      self->first_frame_time = gdk_frame_clock_get_frame_time (frame_clock);
      return G_SOURCE_CONTINUE;
    }

  /* TODO: call for update signals in renderer and others */
  wb_camera_update_matrix (self->camera);
  gtk_widget_queue_draw (widget);

  return G_SOURCE_CONTINUE;
}

static gboolean
wb_window_save_settings (gpointer data)
{
  WbWindow *self = data;
  GdkRectangle geom = {0};
  gboolean maximized;

  g_assert (WB_IS_WINDOW (self));

  self->queued_window_save = 0;

  if (!gtk_widget_get_realized (GTK_WIDGET (self)) ||
      !gtk_widget_get_visible (GTK_WIDGET (self)))
    return G_SOURCE_REMOVE;

  if (settings == NULL)
    settings = g_settings_new ("io.sam.WorldBuilder");

  gtk_window_get_default_size (GTK_WINDOW (self), &geom.width, &geom.height);
  maximized = gtk_window_is_maximized (GTK_WINDOW (self));

  g_settings_set (settings, "window-size", "(ii)", geom.width, geom.height);
  g_settings_set_boolean (settings, "window-maximized", maximized);

  return G_SOURCE_REMOVE;
}

static void
wb_window_size_allocate (GtkWidget *widget,
                         gint       width,
                         gint       height,
                         gint       baseline)
{
  WbWindow *self = (WbWindow *)widget;

  g_assert (WB_IS_WINDOW (self));

  GTK_WIDGET_CLASS (wb_window_parent_class)->size_allocate (widget, width, height, baseline);

  if (self->queued_window_save == 0)
    self->queued_window_save = g_timeout_add_seconds (1, wb_window_save_settings, self);
}

static void
wb_window_restore_size (WbWindow *self,
                        gint      width,
                        gint      height)
{
  g_assert (WB_IS_WINDOW (self));

  gtk_window_set_default_size (GTK_WINDOW (self), width, height);
}

static void
wb_winodw_realize (GtkWidget *widget)
{
  WbWindow *self = (WbWindow *)widget;
  GdkRectangle geom = {0};
  gboolean maximized = FALSE;

  g_assert (WB_IS_WINDOW (self));

  if (settings == NULL)
    settings = g_settings_new ("io.sam.WorldBuilder");

  g_settings_get (settings, "window-size", "(ii)", &geom.width, &geom.height);
  g_settings_get (settings, "window-maximized", "b", &maximized);

  wb_window_restore_size (self, geom.width, geom.height);

  GTK_WIDGET_CLASS (wb_window_parent_class)->realize (widget);

  if (maximized)
    gtk_window_maximize (GTK_WINDOW (self));
}

static gfloat
get_pan_speed (gint viewport_dimension)
{
  gfloat dim;

  dim = MIN (viewport_dimension / 1000.0f, 2.4f);

  return 0.0366f * (dim * dim) - 0.1778f * dim + 0.3021f;
}

static void
on_drag_gesture_drag_update_cb (WbWindow       *self,
                                gdouble         offset_x,
                                gdouble         offset_y,
                                GtkGestureDrag *drag_gesture)
{
  guint mouse_button;

  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_GESTURE_DRAG (drag_gesture));

  mouse_button = gtk_gesture_single_get_current_button (GTK_GESTURE_SINGLE (drag_gesture));

  if (mouse_button == GDK_BUTTON_SECONDARY)
    wb_camera_move (self->camera,
                    offset_x * get_pan_speed (self->gl_viewport.width),
                    - offset_y * get_pan_speed (self->gl_viewport.height),
                    0.0f);

  if (mouse_button == GDK_BUTTON_PRIMARY)
    wb_camera_rotate (self->camera,
                      (gfloat)offset_y / 15.0f / G_PI,
                      (gfloat)offset_x / 15.0f / G_PI);
}

static gboolean
on_key_event_key_pressed_cb (WbWindow              *self,
                             guint                  keyval,
                             guint                  keycode,
                             GdkModifierType        state,
                             GtkEventControllerKey *key_event)
{
  g_assert (WB_IS_WINDOW (self));
  g_assert (GTK_IS_EVENT_CONTROLLER_KEY (key_event));

  switch (keyval)
    {
    case GDK_KEY_a:
      wb_camera_move (self->camera,
                      75.0f * get_pan_speed (self->gl_viewport.width),
                      0.0f, 0.0f);
      break;

    case GDK_KEY_d:
      wb_camera_move (self->camera,
                      - 75.0f * get_pan_speed (self->gl_viewport.width),
                      0.0f, 0.0f);
      break;

    case GDK_KEY_s:
      wb_camera_move (self->camera, 0.0f, 0.0f,
                      - 75.0f * get_pan_speed (self->gl_viewport.height));
      break;

    case GDK_KEY_w:
      wb_camera_move (self->camera, 0.0f, 0.0f,
                      75.0f * get_pan_speed (self->gl_viewport.height));
      break;

    default:
      break;
    }

  return TRUE;
}

static void
wb_window_class_init (WbWindowClass *klass)
{
  GtkWidgetClass * widget_class = GTK_WIDGET_CLASS (klass);

  widget_class->realize = wb_winodw_realize;
  widget_class->size_allocate = wb_window_size_allocate;

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/WorldBuilder/core/wb-window.ui");
  gtk_widget_class_bind_template_child (widget_class, WbWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, WbWindow, notifications);
  gtk_widget_class_bind_template_child (widget_class, WbWindow, overlays);
  gtk_widget_class_bind_template_child (widget_class, WbWindow, viewport);
}

static void
wb_window_init (WbWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect_object (self->viewport, "create-context",
                           G_CALLBACK (on_viewport_create_context_cb),
                           self, G_CONNECT_SWAPPED);
  g_signal_connect_object (self->viewport, "render",
                           G_CALLBACK (on_viewport_render_cb),
                           self, G_CONNECT_SWAPPED);
  g_signal_connect_object (self->viewport, "resize",
                           G_CALLBACK (on_viewport_resize_cb),
                           self, G_CONNECT_SWAPPED);
  g_signal_connect_object (self->viewport, "realize",
                           G_CALLBACK (on_viewport_realize_cb),
                           self, G_CONNECT_SWAPPED);
  g_signal_connect_object (self->viewport, "unrealize",
                           G_CALLBACK (on_viewport_unrealize_cb),
                           self, G_CONNECT_SWAPPED);

  self->tick = gtk_widget_add_tick_callback (GTK_WIDGET (self->viewport),
                                             on_viewport_update_cb,
                                             self, NULL);

  self->click_gesture = (GtkGestureClick *)gtk_gesture_click_new ();
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (self->click_gesture), 0);

  self->drag_gesture = (GtkGestureDrag *)gtk_gesture_drag_new ();
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (self->drag_gesture), 0);

  self->scroll_event = (GtkEventControllerScroll *)gtk_event_controller_scroll_new (GTK_EVENT_CONTROLLER_SCROLL_VERTICAL);
  self->key_event = (GtkEventControllerKey *)gtk_event_controller_key_new ();

  gtk_widget_add_controller (GTK_WIDGET (self->viewport), GTK_EVENT_CONTROLLER (self->click_gesture));
  gtk_widget_add_controller (GTK_WIDGET (self->viewport), GTK_EVENT_CONTROLLER (self->drag_gesture));
  gtk_widget_add_controller (GTK_WIDGET (self->viewport), GTK_EVENT_CONTROLLER (self->scroll_event));
  gtk_widget_add_controller (GTK_WIDGET (self->viewport), GTK_EVENT_CONTROLLER (self->key_event));

  /* g_signal_connect_swapped (self->click_gesture, "pressed", G_CALLBACK (on_click_gesture_pressed_cb), self); */
  g_signal_connect_swapped (self->drag_gesture, "drag-update", G_CALLBACK (on_drag_gesture_drag_update_cb), self);
  /* g_signal_connect_swapped (self->scroll_event, "scroll", G_CALLBACK (on_scroll_event_scroll_cb), self); */
  g_signal_connect_swapped (self->key_event, "key-pressed", G_CALLBACK (on_key_event_key_pressed_cb), self);
}

void
wb_window_notify (WbWindow    *self,
                  const gchar *msg)
{
  g_return_if_fail (WB_IS_WINDOW (self));

  if (wb_str_empty (msg))
    return;

  adw_toast_overlay_add_toast (self->notifications, adw_toast_new (msg));
}

void
wb_window_add_overlay (WbWindow  *self,
                       GtkWidget *child)
{
  g_return_if_fail (WB_IS_WINDOW (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  g_object_set (child,
                "margin-top", 12,
                "margin-bottom", 12,
                "margin-left", 12,
                "margin-right", 12,
                NULL);

  gtk_overlay_add_overlay (self->overlays, child);
}

void
wb_window_remove_overlay (WbWindow  *self,
                          GtkWidget *child)
{
  g_return_if_fail (WB_IS_WINDOW (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  gtk_overlay_remove_overlay (self->overlays, child);
}

