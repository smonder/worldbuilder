/* wb-private.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbPrivate"

#include "config.h"

#include "wb-loader.h"
#include "wb-private.h"


void
_wb_load_game_assets_for_renderer (WbRenderer *renderer)
{
  WbRenderNode * test_cube_asset;

  g_assert (WB_IS_RENDERER (renderer));

  /* Test cube */
  test_cube_asset = wb_loader_load_obj_model (wb_loader_get_default (),
                                              "/io/sam/worldbuilder-assets/cube.obj");
  wb_render_node_set_scale (test_cube_asset, 100.0f, 100.0f, 100.0f);
  wb_renderer_append_node (renderer, test_cube_asset);
}
