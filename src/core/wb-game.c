/* wb-game.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbGame"

#include "config.h"

#include <glib/gi18n.h>

#include "wb-game-private.h"
#include "wb-window.h"

struct _WbGame
{
  AdwApplication parent_instance;

  WbWindow * game_window;
};

G_DEFINE_FINAL_TYPE (WbGame, wb_game, ADW_TYPE_APPLICATION)

static void
wb_game_activate (GApplication *app)
{
  WbGame * self = (WbGame *)app;

  g_assert (WB_IS_GAME (self));

  if (self->game_window == NULL)
    self->game_window = g_object_new (WB_TYPE_WINDOW,
                                      "application", app,
                                      "title", PACKAGE_NAME,
                                      NULL);

  gtk_window_present (GTK_WINDOW (self->game_window));
}

static void
wb_game_class_init (WbGameClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = wb_game_activate;
}

static void
wb_game_init (WbGame *self)
{
  _wb_game_init_actions (self);
}


WbGame *
wb_game_new (void)
{
  return g_object_new (WB_TYPE_GAME,
                       "application-id", "io.sam.WorldBuilder",
                       "flags", G_APPLICATION_FLAGS_NONE,
                       "resource-base-path", "/io/sam/WorldBuilder",
                       NULL);
}

WbWindow *
wb_game_get_main (void)
{
  WbGame * self = WB_GAME_DEFAULT;
  return self->game_window;
}
