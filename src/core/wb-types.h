/* wb-types.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define WB_IS_DRAW_MODE(x) (((x) < WB_DRAW_INVALID_MODE))

typedef enum
{
  WB_INDEX_BUFFER,
  WB_VERTEX_BUFFER,

  N_WB_BUFFER_TYPES
} WbBufferType;

typedef enum
{
  WB_DRAW_POINTS,
  WB_DRAW_LINES,
  WB_DRAW_SOLIDS,
  WB_DRAW_INVALID_MODE,
} WbDrawMode;


G_END_DECLS
