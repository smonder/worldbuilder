/* wb-window.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_WINDOW (wb_window_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbWindow, wb_window, WB, WINDOW, AdwApplicationWindow)

WB_AVAILABLE_IN_ALL
WbWindow * wb_game_get_main (void);

WB_AVAILABLE_IN_ALL
void       wb_window_notify         (WbWindow *    self,
                                     const gchar * msg);

WB_AVAILABLE_IN_ALL
void       wb_window_add_overlay    (WbWindow *    self,
                                     GtkWidget *   child);
WB_AVAILABLE_IN_ALL
void       wb_window_remove_overlay (WbWindow *    self,
                                     GtkWidget *   child);

G_END_DECLS
