/* wb-game.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_GAME (wb_game_get_type())
#define WB_GAME_DEFAULT ((WbGame *)g_application_get_default ())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbGame, wb_game, WB, GAME, AdwApplication)

WB_AVAILABLE_IN_ALL
WbGame *    wb_game_new      (void);

G_END_DECLS
