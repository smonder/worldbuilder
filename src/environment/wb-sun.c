/* wb-sun.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbSun"

#include "config.h"

#include "wb-sun.h"

struct _WbSun
{
  WbLight parent_instance;

  gfloat diffuse;
};

G_DEFINE_FINAL_TYPE (WbSun, wb_sun, WB_TYPE_LIGHT)

enum {
  PROP_0,
  PROP_DIFFUSE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
wb_sun_finalize (GObject *object)
{
  WbSun *self = (WbSun *)object;

  G_OBJECT_CLASS (wb_sun_parent_class)->finalize (object);
}

static void
wb_sun_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  WbSun *self = WB_SUN (object);

  switch (prop_id)
    {
    case PROP_DIFFUSE:
      g_value_set_float (value, wb_sun_get_diffuse (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_sun_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  WbSun *self = WB_SUN (object);

  switch (prop_id)
    {
    case PROP_DIFFUSE:
      wb_sun_set_diffuse (self, g_value_get_float (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_sun_class_init (WbSunClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_sun_finalize;
  object_class->get_property = wb_sun_get_property;
  object_class->set_property = wb_sun_set_property;

  properties [PROP_DIFFUSE] =
    g_param_spec_float ("diffuse",
                        "Diffuse",
                        "The diffuse percent of the sun.",
                        0.0f, 1.0f, 1.0f,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
wb_sun_init (WbSun *self)
{
  wb_light_set_position (WB_LIGHT (self), &(graphene_point3d_t){ - 400.0f, 400.0f, 400.0f });
  wb_light_set_color (WB_LIGHT (self), &(GdkRGBA){ 1.0f, 1.0f, 1.0f, 1.0f });

  self->diffuse = 1.0f;
}


WbLight *
wb_sun_new (void)
{
  return g_object_new (WB_TYPE_SUN, NULL);
}

gfloat
wb_sun_get_diffuse (WbSun *self)
{
  g_return_val_if_fail (WB_IS_SUN (self), 0.0f);
  return self->diffuse;
}

void
wb_sun_set_diffuse (WbSun  *self,
                    gfloat  setting)
{
  g_return_if_fail (WB_IS_SUN (self));

  if (setting > 1.0f || setting < 0.0f)
    CLAMP (setting, 0.0f, 1.0f);

  if (self->diffuse != setting)
    {
      self->diffuse = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DIFFUSE]);
    }
}
