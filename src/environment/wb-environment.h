/* wb-environment.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "wb-camera.h"
#include "wb-environment-shader.h"
#include "wb-sun.h"
#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_ENVIRONMENT (wb_environment_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbEnvironment, wb_environment, WB, ENVIRONMENT, GObject)

WB_AVAILABLE_IN_ALL
WbEnvironment * wb_environment_new               (void) G_GNUC_WARN_UNUSED_RESULT;

WB_AVAILABLE_IN_ALL
void            wb_environment_prepare           (WbEnvironment * self);
WB_AVAILABLE_IN_ALL
void            wb_environment_render            (WbEnvironment * self,
                                                  WbCamera *      camera);

WB_AVAILABLE_IN_ALL
GdkRGBA *       wb_environment_get_horizon_color (WbEnvironment * self);
WB_AVAILABLE_IN_ALL
void            wb_environment_set_horizon_color (WbEnvironment * self,
                                                  GdkRGBA *       color);

WB_AVAILABLE_IN_ALL
WbLight *       wb_environment_get_sun           (WbEnvironment * self);

G_END_DECLS

