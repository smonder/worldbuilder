/* wb-environment.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbEnvironment"

#include "config.h"

#include "wb-environment.h"
#include "wb-environment-shader.h"
#include "wb-render-node.h"
#include "wb-cubemap-texture.h"
#include "wb-vertex.h"

#define SKY_CUBE_SIZE 10000.0f

struct _WbEnvironment
{
  GObject parent_instance;

  WbLight * sun;
  GdkRGBA horizon_color;

  WbShader * shader;
  WbCubemapTexture * sky_texture;
  WbRenderNode * cube;

  graphene_matrix_t * view_matrix;
};

G_DEFINE_FINAL_TYPE (WbEnvironment, wb_environment, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_SUN,
  PROP_HORIZON_COLOR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static WbVertex *
create_cube (WbVertex     *iter,
             guint        *index_buffer,
             const gfloat  size)
{
  guint index_data [36] =
    {
      0, 1, 2, 2, 3, 0,
      4, 1, 0, 0, 5, 4,
      2, 6, 7, 7, 3, 2,
      4, 5, 7, 7, 6, 4,
      0, 3, 7, 7, 5, 0,
      1, 4, 2, 2, 4, 6
    };

  g_return_val_if_fail (iter, NULL);
  g_return_val_if_fail (size > 0, NULL);


  iter->position = (graphene_point3d_t){ -size,  size, -size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ -size, -size, -size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ size, -size, -size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ size,  size, -size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ -size, -size,  size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ -size,  size,  size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ size, -size,  size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  iter->position = (graphene_point3d_t){ size,  size,  size };
  iter->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
  iter->texture_uv = (graphene_point_t){ 0.0f, 0.0f};
  iter->texture_id = 0;
  iter ++;

  memcpy (index_buffer, index_data, sizeof (index_data));

  return iter;
}

WbEnvironment *
wb_environment_new (void)
{
  return g_object_new (WB_TYPE_ENVIRONMENT, NULL);
}

static void
wb_environment_finalize (GObject *object)
{
  WbEnvironment *self = (WbEnvironment *)object;

  g_clear_object (&self->cube);
  g_clear_object (&self->sky_texture);
  g_clear_object (&self->shader);

  g_clear_object (&self->sun);

  G_OBJECT_CLASS (wb_environment_parent_class)->finalize (object);
}

static void
wb_environment_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  WbEnvironment *self = WB_ENVIRONMENT (object);

  switch (prop_id)
    {
    case PROP_HORIZON_COLOR:
      g_value_set_boxed (value, wb_environment_get_horizon_color (self));
      break;

    case PROP_SUN:
      g_value_set_object (value, wb_environment_get_sun (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_environment_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  WbEnvironment *self = WB_ENVIRONMENT (object);

  switch (prop_id)
    {
    case PROP_HORIZON_COLOR:
      wb_environment_set_horizon_color (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_environment_class_init (WbEnvironmentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_environment_finalize;
  object_class->get_property = wb_environment_get_property;
  object_class->set_property = wb_environment_set_property;

  properties [PROP_SUN] =
    g_param_spec_object ("sun",
                         "Sun",
                         "The sun light object.",
                         WB_TYPE_SUN,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_HORIZON_COLOR] =
    g_param_spec_boxed ("horizon-color",
                        "Horizon Color",
                        "The color of the horizon.",
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
wb_environment_init (WbEnvironment *self)
{
  const gchar *faces_resource_path [] = {
    "/io/sam/worldbuilder-textures/mountains-sea/right.jpg",
    "/io/sam/worldbuilder-textures/mountains-sea/left.jpg",
    "/io/sam/worldbuilder-textures/mountains-sea/top.jpg",
    "/io/sam/worldbuilder-textures/mountains-sea/bottom.jpg",
    "/io/sam/worldbuilder-textures/mountains-sea/front.jpg",
    "/io/sam/worldbuilder-textures/mountains-sea/back.jpg"
  };

  self->shader = wb_environment_shader_new ();
  self->sky_texture = wb_cubemap_texture_new (faces_resource_path);

  self->view_matrix = graphene_matrix_alloc ();
  graphene_matrix_init_identity (self->view_matrix);

  self->sun = wb_sun_new ();
  self->horizon_color = (GdkRGBA){ 1.0f, 0.0f, 0.0f, 1.0f };
}

void
wb_environment_prepare (WbEnvironment *self)
{
  WbVertex cube_buffer [8];
  WbVertex * cube = cube_buffer;
  guint index_buffer [36];

  g_return_if_fail (WB_IS_ENVIRONMENT (self));

  cube = create_cube (cube, index_buffer, SKY_CUBE_SIZE);

  self->cube = wb_render_node_new ("Sky", G_N_ELEMENTS (index_buffer));
  wb_render_node_bind (self->cube);
  wb_render_node_set_buffer_data (self->cube, WB_VERTEX_BUFFER, 0,
                                  sizeof (cube_buffer), cube_buffer);
  wb_render_node_set_buffer_data (self->cube, WB_INDEX_BUFFER, 0,
                                  sizeof (index_buffer), index_buffer);
  wb_render_node_unbind (self->cube);
}

void
wb_environment_render (WbEnvironment *self,
                       WbCamera      *camera)
{
  g_return_if_fail (WB_IS_ENVIRONMENT (self));
  g_return_if_fail (WB_IS_CAMERA (camera));
  gfloat mat [16];

  wb_shader_bind (self->shader);

  graphene_matrix_to_float (wb_camera_get_matrix (camera), mat);
  mat [12] = 0.0f;
  mat [13] = 0.0f;
  mat [14] = 0.0f;

  graphene_matrix_init_from_float (self->view_matrix, mat);

  wb_shader_set_umatrix (self->shader, "u_ViewMatrix",
                         self->view_matrix);
  wb_shader_set_umatrix (self->shader, "u_ProjectionMatrix",
                         wb_camera_get_projection (camera));
  wb_shader_set_samplers (self->shader, "u_Textures", 1);
  wb_shader_set_urgba (self->shader, "u_HorizonColor", &self->horizon_color);

  wb_cubemap_texture_bind (self->sky_texture);

  wb_render_node_bind (self->cube);
  wb_render_node_bind_ibuffer (self->cube);

  epoxy_glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
  epoxy_glDrawElements (GL_TRIANGLES,
                        wb_render_node_get_capacity (self->cube),
                        GL_UNSIGNED_INT, NULL);

  wb_render_node_unbind_ibuffer (self->cube);
  wb_render_node_unbind (self->cube);
  wb_cubemap_texture_unbind (self->sky_texture);
  wb_shader_unbind (self->shader);
}

GdkRGBA *
wb_environment_get_horizon_color (WbEnvironment *self)
{
  g_return_val_if_fail (WB_IS_ENVIRONMENT (self), NULL);
  return &self->horizon_color;
}

void
wb_environment_set_horizon_color (WbEnvironment *self,
                                  GdkRGBA       *color)
{
  g_return_if_fail (WB_IS_ENVIRONMENT (self));
  g_return_if_fail (color);

  if (!gdk_rgba_equal (color, &self->horizon_color))
    {
      self->horizon_color.red = color->red;
      self->horizon_color.green = color->green;
      self->horizon_color.blue = color->blue;
      self->horizon_color.alpha = color->alpha;

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_HORIZON_COLOR]);
    }
}

WbLight *
wb_environment_get_sun (WbEnvironment *self)
{
  g_return_val_if_fail (WB_IS_ENVIRONMENT (self), NULL);
  return self->sun;
}
