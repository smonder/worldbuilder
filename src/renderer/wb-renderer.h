/* wb-renderer.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "wb-shader.h"
#include "wb-environment.h"
#include "wb-camera.h"
#include "wb-render-node.h"
#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_RENDERER (wb_renderer_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (WbRenderer, wb_renderer, WB, RENDERER, GObject)

struct _WbRendererClass
{
  GObjectClass parent_class;

  void         (* render_setup) (WbRenderer *    renderer,
                                 WbShader *      shader,
                                 WbEnvironment * environment,
                                 WbCamera *      camera);
  void         (* render_node)  (WbRenderer *    renderer,
                                 WbRenderNode *  node,
                                 WbShader *      shader,
                                 WbEnvironment * environment,
                                 WbCamera *      camera);

  void         (* update_node)  (WbRenderer *    renderer,
                                 WbRenderNode *  node,
                                 guint           time_step);
};

WB_AVAILABLE_IN_ALL
void         wb_renderer_append_node  (WbRenderer *   self,
                                       WbRenderNode * node);
WB_AVAILABLE_IN_ALL
void         wb_renderer_prepend_node (WbRenderer *   self,
                                       WbRenderNode * node);
WB_AVAILABLE_IN_ALL
void         wb_renderer_remove_node  (WbRenderer *   self,
                                       WbRenderNode * node);

WB_AVAILABLE_IN_ALL
void         wb_renderer_render_all   (WbRenderer *   self,
                                       WbShader *     shader,
                                       WbEnvironment *environment,
                                       WbCamera *     camera);
WB_AVAILABLE_IN_ALL
void         wb_renderer_update_all   (WbRenderer *   self,
                                       guint          time_step);

G_END_DECLS
