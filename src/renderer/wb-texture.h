/* wb-texture.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_TEXTURE (wb_texture_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbTexture, wb_texture, WB, TEXTURE, GObject)

WB_AVAILABLE_IN_ALL
WbTexture *    wb_texture_new            (const gchar * resource_path) G_GNUC_WARN_UNUSED_RESULT;
WB_AVAILABLE_IN_ALL
WbTexture *    wb_texture_new_from_file  (const gchar * file_path) G_GNUC_WARN_UNUSED_RESULT;

WB_AVAILABLE_IN_ALL
GdkRectangle * wb_texture_get_dimensions (WbTexture *   self);

WB_AVAILABLE_IN_ALL
guint          wb_texture_get_slot       (WbTexture *   self) G_GNUC_CONST;
WB_AVAILABLE_IN_ALL
void           wb_texture_bind_to_slot   (WbTexture *   self,
                                          const guint   slot);
WB_AVAILABLE_IN_ALL
void           wb_texture_bind           (WbTexture *   self);
WB_AVAILABLE_IN_ALL
void           wb_texture_unbind         (WbTexture *   self);



G_END_DECLS
