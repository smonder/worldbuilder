/* wb-render-node.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <graphene-gobject.h>

#include "wb-types.h"
#include "wb-texture.h"
#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_RENDER_NODE (wb_render_node_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbRenderNode, wb_render_node, WB, RENDER_NODE, GObject)


WB_AVAILABLE_IN_ALL
WbRenderNode * wb_render_node_new          (const gchar *  name,
                                            const guint    capacity) G_GNUC_WARN_UNUSED_RESULT;

WB_AVAILABLE_IN_ALL
gchar *     wb_render_node_get_name        (WbRenderNode * self);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_name        (WbRenderNode * self,
                                            const gchar *  setting);

WB_AVAILABLE_IN_ALL
guint       wb_render_node_get_capacity    (WbRenderNode * self) G_GNUC_CONST;

WB_AVAILABLE_IN_ALL
WbDrawMode  wb_render_node_get_solids_mode (WbRenderNode * self);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_solids_mode (WbRenderNode * self,
                                            WbDrawMode     setting);

WB_AVAILABLE_IN_ALL
graphene_matrix_t * wb_render_node_get_matrix (WbRenderNode * self);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_position    (WbRenderNode * self,
                                            const gfloat   x_position,
                                            const gfloat   y_position,
                                            const gfloat   z_position);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_rotation    (WbRenderNode * self,
                                            const gfloat   x_degree,
                                            const gfloat   y_degree,
                                            const gfloat   z_degree);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_scale       (WbRenderNode * self,
                                            const gfloat   x_scale,
                                            const gfloat   y_scale,
                                            const gfloat   z_scale);

WB_AVAILABLE_IN_ALL
WbTexture * wb_render_node_get_texture     (WbRenderNode * self,
                                            const guint    slot);
WB_AVAILABLE_IN_ALL
void        wb_render_node_set_texture     (WbRenderNode * self,
                                            const guint    slot,
                                            WbTexture *    texture);
WB_AVAILABLE_IN_ALL
void        wb_render_node_bind_textures   (WbRenderNode * self);

WB_AVAILABLE_IN_ALL
void        wb_render_node_bind            (WbRenderNode * self);
WB_AVAILABLE_IN_ALL
void        wb_render_node_unbind          (WbRenderNode * self);

WB_AVAILABLE_IN_ALL
void        wb_render_node_bind_ibuffer    (WbRenderNode * self);
WB_AVAILABLE_IN_ALL
void        wb_render_node_unbind_ibuffer  (WbRenderNode * self);

WB_AVAILABLE_IN_ALL
void        wb_render_node_set_buffer_data (WbRenderNode * self,
                                            WbBufferType   buffer,
                                            const guint    offset,
                                            const gsize    data_size,
                                            gconstpointer  data);

G_END_DECLS
