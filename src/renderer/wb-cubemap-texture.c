/* wb-cubemap-texture.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbCubemapTexture"

#include "config.h"

#include <glib/gi18n.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <epoxy/gl.h>

#include "wb-macros.h"
#include "wb-cubemap-texture.h"


struct _WbCubemapTexture
{
  GObject parent_instance;

  /* We need a unique ID to be generated and used by OpenGL API. */
  guint ID_;
};

G_DEFINE_FINAL_TYPE (WbCubemapTexture, wb_cubemap_texture, G_TYPE_OBJECT)


static void
wb_cubemap_texture_finalize (GObject *object)
{
  WbCubemapTexture *self = (WbCubemapTexture *)object;

  epoxy_glDeleteTextures (1, &self->ID_);

  G_OBJECT_CLASS (wb_cubemap_texture_parent_class)->finalize (object);
}

static void
wb_cubemap_texture_class_init (WbCubemapTextureClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_cubemap_texture_finalize;
}

static void
wb_cubemap_texture_init (WbCubemapTexture *self)
{
}


WbCubemapTexture *
wb_cubemap_texture_new (const gchar **faces_resource_path)
{
  g_autoptr (WbCubemapTexture) self = NULL;
  GdkPixbuf *pixbuf = NULL;
  GError *error = NULL;
  gint width, height;
  guchar * data;

  self = g_object_new (WB_TYPE_CUBEMAP_TEXTURE, NULL);

  epoxy_glGenTextures (1, &self->ID_);
  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, self->ID_);

  for (guint i = 0; i < 6; i++)
    {
      pixbuf = gdk_pixbuf_new_from_resource (faces_resource_path [i], &error);

      if (!pixbuf)
        {
          g_critical ("Failed to load image file. %s", error->message);
          return NULL;
        }

      width = gdk_pixbuf_get_width (pixbuf);
      height = gdk_pixbuf_get_height (pixbuf);
      data = gdk_pixbuf_get_pixels (pixbuf);

      epoxy_glTexImage2D (GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                          0, GL_RGB, width, height, 0, GL_RGB,
                          GL_UNSIGNED_BYTE, data);

      g_object_unref (pixbuf);
    }

  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  /* epoxy_glGenerateMipmap (GL_TEXTURE_CUBE_MAP); */
  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, 0);

  return g_steal_pointer (&self);
}

WbCubemapTexture *
wb_cubemap_texture_new_from_files (const gchar **faces_files_path)
{
  g_autoptr (WbCubemapTexture) self = NULL;
  GdkPixbuf *pixbuf = NULL;
  GError *error = NULL;
  gint width, height;
  guchar * data;

  self = g_object_new (WB_TYPE_CUBEMAP_TEXTURE, NULL);

  epoxy_glGenTextures (1, &self->ID_);
  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, self->ID_);

  for (guint i = 0; i < 6; i++)
    {
      pixbuf = gdk_pixbuf_new_from_file (faces_files_path [i], &error);

      if (!pixbuf)
        {
          g_critical ("Failed to load image file. %s", error->message);
          return NULL;
        }

      width = gdk_pixbuf_get_width (pixbuf);
      height = gdk_pixbuf_get_height (pixbuf);
      data = gdk_pixbuf_get_pixels (pixbuf);

      epoxy_glTexImage2D (GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                          0, GL_RGB, width, height, 0, GL_RGB,
                          GL_UNSIGNED_BYTE, data);

      g_object_unref (pixbuf);
    }

  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  epoxy_glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  /* epoxy_glGenerateMipmap (GL_TEXTURE_CUBE_MAP); */
  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, 0);

  return g_steal_pointer (&self);
}

void
wb_cubemap_texture_bind (WbCubemapTexture *self)
{
  g_return_if_fail (WB_IS_CUBEMAP_TEXTURE (self));

  if (self->ID_ == 0)
    {
      g_critical ("Failed to bind GL Texture: Texture is either invalid or not prepared successfully.");
      return;
    }

  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, self->ID_);
}

void
wb_cubemap_texture_unbind (WbCubemapTexture *self)
{
  epoxy_glBindTexture (GL_TEXTURE_CUBE_MAP, 0);
}
