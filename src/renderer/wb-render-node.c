/* wb-render-node.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbRenderNode"

#include "config.h"

#include <glib/gi18n.h>
#include <epoxy/gl.h>

#include "wb-enums.h"
#include "wb-macros.h"
#include "wb-render-node-private.h"
#include "wb-vertex.h"


#define MAX_TEXTURE_SLOTS 32

struct _WbRenderNode
{
  GObject parent_instance;

  GList   link_;

  gchar * name;

  /* We need a unique ID to be generated and used by OpenGL API. */
  guint ID_;

  /* The capacity of the node is how many vertices can this node hold */
  guint node_capacity;

  /* The solids representation mode */
  WbDrawMode solids_mode;

  /* We can only bind @MAX_TEXTURE_SLOTS@ textures to each render node */
  WbTexture * textures [MAX_TEXTURE_SLOTS];

  /* We only need to store th ID of both index and vertex buffer since we only
   * need them on the GPU side and we don't need any data on the CPU.
   */
  guint buffers [N_WB_BUFFER_TYPES];

  /* node transformations */
  graphene_matrix_t * matrix;
  graphene_point3d_t position;
  graphene_point3d_t rotation;
  graphene_point3d_t scale;
};

G_DEFINE_FINAL_TYPE (WbRenderNode, wb_render_node, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_CAPACITY,
  PROP_SOLIDS_MODE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
update_node_matrix (WbRenderNode *self)
{
  g_assert (WB_IS_RENDER_NODE (self));

  graphene_matrix_init_identity (self->matrix);
  graphene_matrix_translate (self->matrix, &self->position);
  graphene_matrix_rotate_x (self->matrix, self->rotation.x);
  graphene_matrix_rotate_y (self->matrix, self->rotation.y);
  graphene_matrix_rotate_z (self->matrix, self->rotation.z);
  graphene_matrix_scale (self->matrix, self->scale.x, self->scale.y, self->scale.z);
}

static void
wb_render_node_finalize (GObject *object)
{
  WbRenderNode *self = (WbRenderNode *)object;

  for (guint i = 1; i < MAX_TEXTURE_SLOTS; i++)
    g_clear_object (&self->textures [i]);

  epoxy_glDeleteBuffers (N_WB_BUFFER_TYPES, self->buffers);
  epoxy_glDeleteVertexArrays (1, &self->ID_);

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (wb_render_node_parent_class)->finalize (object);
}

static void
wb_render_node_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  WbRenderNode *self = WB_RENDER_NODE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, wb_render_node_get_name (self));
      break;

    case PROP_CAPACITY:
      g_value_set_uint (value, self->node_capacity);
      break;

    case PROP_SOLIDS_MODE:
      g_value_set_enum (value, wb_render_node_get_solids_mode (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_render_node_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  WbRenderNode *self = WB_RENDER_NODE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      wb_render_node_set_name (self, g_value_get_string (value));
      break;

    case PROP_SOLIDS_MODE:
      wb_render_node_set_solids_mode (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_render_node_class_init (WbRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_render_node_finalize;
  object_class->get_property = wb_render_node_get_property;
  object_class->set_property = wb_render_node_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the object in the node.",
                         _("dummy"),
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_SOLIDS_MODE] =
    g_param_spec_enum ("solids-mode",
                       "Solids Mode",
                       "The mode in which solids are rendered.",
                       WB_TYPE_DRAW_MODE, WB_DRAW_SOLIDS,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  properties [PROP_CAPACITY] =
    g_param_spec_uint ("capacity",
                       "Capacity",
                       "The capacity of the node.",
                       0, G_MAXUINT, 0,
                       (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
wb_render_node_init (WbRenderNode *self)
{
  self->link_.data = self;
  self->name = g_strdup (_("dummy"));

  self->matrix = graphene_matrix_alloc ();
  graphene_matrix_init_identity (self->matrix);

  self->position = (graphene_point3d_t){ 0.0f, 0.0f, 0.0f };
  self->rotation = (graphene_point3d_t){ 0.0f, 0.0f, 0.0f };
  self->scale = (graphene_point3d_t){ 0.0f, 0.0f, 0.0f };

  self->solids_mode = WB_DRAW_SOLIDS;
}


WbRenderNode *
wb_render_node_new (const gchar *name,
                    const guint  capacity)
{
  g_autoptr (WbRenderNode) self = NULL;

  self = g_object_new (WB_TYPE_RENDER_NODE,
                       "name", name, NULL);

  epoxy_glCreateVertexArrays (1, &self->ID_);
  epoxy_glBindVertexArray (self->ID_);

  epoxy_glCreateBuffers (N_WB_BUFFER_TYPES, self->buffers);
  epoxy_glBindBuffer (GL_ARRAY_BUFFER, self->buffers [WB_VERTEX_BUFFER]);
  epoxy_glBufferData (GL_ARRAY_BUFFER, capacity * sizeof (WbVertex), NULL, GL_DYNAMIC_DRAW);

  wb_vertex_setup_layout (self->ID_);

  epoxy_glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, self->buffers [WB_INDEX_BUFFER]);
  epoxy_glBufferData (GL_ELEMENT_ARRAY_BUFFER, (capacity + 2) * sizeof (guint), NULL, GL_DYNAMIC_DRAW);

  epoxy_glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
  epoxy_glBindVertexArray (0);

  self->node_capacity = capacity;

  return g_steal_pointer (&self);
}

gchar *
wb_render_node_get_name (WbRenderNode *self)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), NULL);
  return self->name;
}

void
wb_render_node_set_name (WbRenderNode *self,
                         const gchar  *setting)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  if (!wb_str_equal (self->name, setting))
    {
      g_clear_pointer (&self->name, g_free);
      self->name = g_strdup (setting);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

guint
wb_render_node_get_capacity (WbRenderNode *self)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), 0);
  return self->node_capacity;
}

WbDrawMode
wb_render_node_get_solids_mode (WbRenderNode *self)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), WB_DRAW_INVALID_MODE);
  return self->solids_mode;
}

void
wb_render_node_set_solids_mode (WbRenderNode *self,
                                WbDrawMode    setting)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));
  g_return_if_fail (WB_IS_DRAW_MODE (setting));

  if (self->solids_mode != setting)
    {
      self->solids_mode = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SOLIDS_MODE]);
    }
}

WbTexture *
wb_render_node_get_texture (WbRenderNode *self,
                            const guint   slot)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), NULL);
  g_return_val_if_fail (slot < MAX_TEXTURE_SLOTS, NULL);

  return self->textures [slot];
}

void
wb_render_node_set_texture (WbRenderNode *self,
                            const guint   slot,
                            WbTexture    *texture)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));
  g_return_if_fail (WB_IS_TEXTURE (texture));
  g_return_if_fail (slot > 0 && slot < MAX_TEXTURE_SLOTS);

  if (g_set_object (&self->textures [slot], texture))
    wb_texture_bind_to_slot (texture, slot);
}

void
wb_render_node_bind_textures (WbRenderNode *self)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  for (guint i = 1; i < MAX_TEXTURE_SLOTS; i ++)
    {
      if (WB_IS_TEXTURE (self->textures [i]))
        wb_texture_bind (self->textures [i]);
    }
}

void
wb_render_node_bind (WbRenderNode *self)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  epoxy_glBindVertexArray (self->ID_);
}

void
wb_render_node_unbind (WbRenderNode *self)
{
  epoxy_glBindVertexArray (0);
}

void
wb_render_node_bind_ibuffer (WbRenderNode *self)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  epoxy_glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, self->buffers [WB_INDEX_BUFFER]);
}

void
wb_render_node_unbind_ibuffer (WbRenderNode *self)
{
  epoxy_glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
}

void
wb_render_node_set_buffer_data (WbRenderNode  *self,
                                WbBufferType   buffer,
                                const guint    offset,
                                const gsize    data_size,
                                gconstpointer  data)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  switch (buffer)
    {
    case WB_INDEX_BUFFER:
      epoxy_glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, self->buffers [WB_INDEX_BUFFER]);
      epoxy_glBufferSubData (GL_ELEMENT_ARRAY_BUFFER, offset, data_size, data);
      break;

    case WB_VERTEX_BUFFER:
      epoxy_glBindBuffer (GL_ARRAY_BUFFER, self->buffers [WB_VERTEX_BUFFER]);
      epoxy_glBufferSubData (GL_ARRAY_BUFFER, offset, data_size, data);
      break;

    case N_WB_BUFFER_TYPES: g_assert_not_reached ();
    default: g_assert_not_reached ();
    }
}


graphene_matrix_t *
wb_render_node_get_matrix (WbRenderNode *self)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), NULL);

  update_node_matrix (self);
  return self->matrix;
}

void
wb_render_node_set_position (WbRenderNode *self,
                             const gfloat  x_position,
                             const gfloat  y_position,
                             const gfloat  z_position)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  self->position.x = x_position;
  self->position.y = y_position;
  self->position.z = z_position;

  update_node_matrix (self);
}

void
wb_render_node_set_rotation (WbRenderNode *self,
                             const gfloat  x_degree,
                             const gfloat  y_degree,
                             const gfloat  z_degree)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  self->rotation.x = x_degree;
  self->rotation.y = y_degree;
  self->rotation.z = z_degree;

  update_node_matrix (self);
}

void
wb_render_node_set_scale (WbRenderNode *self,
                          const gfloat  x_scale,
                          const gfloat  y_scale,
                          const gfloat  z_scale)
{
  g_return_if_fail (WB_IS_RENDER_NODE (self));

  self->scale.x = x_scale;
  self->scale.y = y_scale;
  self->scale.z = z_scale;

  update_node_matrix (self);
}

GList *
_wb_render_node_get_link (WbRenderNode *self)
{
  g_return_val_if_fail (WB_IS_RENDER_NODE (self), NULL);
  return &self->link_;
}

