/* wb-texture.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbTexture"

#include "config.h"

#include <glib/gi18n.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <epoxy/gl.h>

#include "wb-macros.h"
#include "wb-texture.h"

struct _WbTexture
{
  GObject parent_instance;

  guint ID_;

  /* We need to keep a copy of the buffer of the image. */
  guchar * texture_buffer;

  /* And the original size of the image used as texture. */
  GdkRectangle dimensions;

  /* A texture slot to sample the texture from */
  guint texture_slot;
};

G_DEFINE_FINAL_TYPE (WbTexture, wb_texture, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_DIMENSIONS,
  PROP_SLOT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
wb_texture_prepare (WbTexture *self)
{
  g_assert (WB_IS_TEXTURE (self));

  glGenTextures (1, &self->ID_);
  glBindTexture (GL_TEXTURE_2D, self->ID_);
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); */
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                      self->dimensions.width, self->dimensions.height,
                      0, GL_RGBA, GL_UNSIGNED_BYTE,
                      self->texture_buffer);
  glGenerateMipmap (GL_TEXTURE_2D);
  glBindTexture (GL_TEXTURE_2D, 0);
}

static void
wb_texture_finalize (GObject *object)
{
  WbTexture *self = (WbTexture *)object;

  glDeleteTextures (1, &self->ID_);

  g_clear_pointer (&self->texture_buffer, g_free);

  G_OBJECT_CLASS (wb_texture_parent_class)->finalize (object);
}

static void
wb_texture_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  WbTexture *self = WB_TEXTURE (object);

  switch (prop_id)
    {
    case PROP_DIMENSIONS:
      g_value_set_boxed (value, wb_texture_get_dimensions (self));
      break;

    case PROP_SLOT:
      g_value_set_uint (value, wb_texture_get_slot (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_texture_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  WbTexture *self = WB_TEXTURE (object);

  switch (prop_id)
    {
    case PROP_SLOT:
      wb_texture_bind_to_slot (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_texture_class_init (WbTextureClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_texture_finalize;
  object_class->get_property = wb_texture_get_property;
  object_class->set_property = wb_texture_set_property;

  /**
   * ForgerTexture:dimensions:
   *
   * The "dimensions" property is the width and height of the texture image.
   */
  properties [PROP_DIMENSIONS] =
    g_param_spec_boxed ("dimensions",
                        "Dimensions",
                        N_("The dimensions of the texture image."),
                        GDK_TYPE_RECTANGLE,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerTexture:slot:
   *
   * The "slot" property is the texture slot in the GPU to bind @self to.
   *
   * Note that max number of texture slots is varrying according to the GPU
   * manufactorer, therefore it is a good practice to check to see how many
   * slots are there using the api.
   *
   * slot 0 is reserved for color;
   */
  properties [PROP_SLOT] =
    g_param_spec_uint ("slot",
                       "Slot",
                       _("Texture slot to bind to."),
                       1, 31, 1,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
wb_texture_init (WbTexture *self)
{
  self->dimensions.x = 0;
  self->dimensions.y = 0;
  self->dimensions.width = 0;
  self->dimensions.height = 0;

  self->texture_slot = 1;
}


WbTexture *
wb_texture_new (const gchar *resource_path)
{
  g_autoptr (WbTexture) self = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;
  GError *error = NULL;

  g_return_val_if_fail (!wb_str_empty (resource_path), NULL);

  pixbuf = gdk_pixbuf_new_from_resource (resource_path, &error);
  if (!pixbuf)
    {
      g_critical ("Failed to load image file. %s", error->message);
      return NULL;
    }

  self = g_object_new (WB_TYPE_TEXTURE, NULL);

  self->dimensions.width = gdk_pixbuf_get_width (pixbuf);
  self->dimensions.height = gdk_pixbuf_get_height (pixbuf);
  self->texture_buffer = gdk_pixbuf_get_pixels (gdk_pixbuf_flip (pixbuf, FALSE));

  wb_texture_prepare (self);

  return g_steal_pointer (&self);
}

WbTexture *
wb_texture_new_from_file (const gchar *file_path)
{
  g_autoptr (WbTexture) self = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;
  GError *error = NULL;

  g_return_val_if_fail (!wb_str_empty (file_path), NULL);

  pixbuf = gdk_pixbuf_new_from_file (file_path, &error);
  if (!pixbuf)
    {
      g_critical ("Failed to load image file. %s", error->message);
      return NULL;
    }

  self = g_object_new (WB_TYPE_TEXTURE, NULL);

  self->dimensions.width = gdk_pixbuf_get_width (pixbuf);
  self->dimensions.height = gdk_pixbuf_get_height (pixbuf);
  self->texture_buffer = gdk_pixbuf_get_pixels (gdk_pixbuf_flip (pixbuf, FALSE));

  wb_texture_prepare (self);

  return g_steal_pointer (&self);
}

GdkRectangle *
wb_texture_get_dimensions (WbTexture *self)
{
  g_return_val_if_fail (WB_IS_TEXTURE (self), NULL);
  return &self->dimensions;
}

guint
wb_texture_get_slot (WbTexture *self)
{
  g_return_val_if_fail (WB_IS_TEXTURE (self), -1);
  return self->texture_slot;
}

void
wb_texture_bind_to_slot (WbTexture   *self,
                         const guint  slot)
{
  g_return_if_fail (WB_IS_TEXTURE (self));
  g_return_if_fail (0 < slot && slot < 32);

  self->texture_slot = slot;
  wb_texture_bind (self);
}

void
wb_texture_bind (WbTexture *self)
{
  g_return_if_fail (WB_IS_TEXTURE (self));

  if (self->ID_ == 0)
    {
      g_critical ("Failed to bind Texture: Texture is either invalid or not prepared successfully.");
      return;
    }

  glBindTextureUnit (self->texture_slot, self->ID_);
}

void
wb_texture_unbind (WbTexture *self)
{
  g_return_if_fail (WB_IS_TEXTURE (self));
  glBindTextureUnit (self->texture_slot, 0);
}
