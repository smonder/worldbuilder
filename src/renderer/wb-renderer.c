/* wb-renderer.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbRenderer"

#include "config.h"

#include "wb-renderer.h"
#include "wb-render-node-private.h"

typedef struct
{
  GQueue nodes;
} WbRendererPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (WbRenderer, wb_renderer, G_TYPE_OBJECT)


typedef struct
{
  WbRenderer *    self;
  WbShader *      shader;
  WbEnvironment * environment;
  WbCamera *      camera;
} RenderForeachData;

typedef struct
{
  WbRenderer * self;
  guint        time_step;
} UpdateForeachData;

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
wb_renderer_finalize (GObject *object)
{
  WbRenderer *self = (WbRenderer *)object;
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);

  G_OBJECT_CLASS (wb_renderer_parent_class)->finalize (object);
}

static void
wb_renderer_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  WbRenderer *self = WB_RENDERER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_renderer_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  WbRenderer *self = WB_RENDERER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_renderer_class_init (WbRendererClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_renderer_finalize;
  object_class->get_property = wb_renderer_get_property;
  object_class->set_property = wb_renderer_set_property;
}

static void
wb_renderer_init (WbRenderer *self)
{
}

void
wb_renderer_append_node (WbRenderer   *self,
                         WbRenderNode *node)
{
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);

  g_return_if_fail (WB_IS_RENDERER (self));
  g_return_if_fail (WB_IS_RENDER_NODE (node));

  g_queue_push_tail_link (&priv->nodes, _wb_render_node_get_link (node));
}

void
wb_renderer_prepend_node (WbRenderer   *self,
                          WbRenderNode *node)
{
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);

  g_return_if_fail (WB_IS_RENDERER (self));
  g_return_if_fail (WB_IS_RENDER_NODE (node));

  g_queue_push_head_link (&priv->nodes, _wb_render_node_get_link (node));
}

void
wb_renderer_remove_node (WbRenderer   *self,
                         WbRenderNode *node)
{
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);

  g_return_if_fail (WB_IS_RENDERER (self));
  g_return_if_fail (WB_IS_RENDER_NODE (node));

  g_queue_remove (&priv->nodes, node);
}



static void
render_foreach_func (WbRenderNode *node,
                     gpointer      user_data)
{
  RenderForeachData * data = user_data;

  g_assert (WB_IS_RENDERER (data->self));
  g_assert (WB_IS_RENDER_NODE (node));

  WB_RENDERER_GET_CLASS (data->self)->render_node (data->self,
                                                   node,
                                                   data->shader,
                                                   data->environment,
                                                   data->camera);
}

void
wb_renderer_render_all (WbRenderer    *self,
                        WbShader      *shader,
                        WbEnvironment *environment,
                        WbCamera      *camera)
{
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);
  RenderForeachData data;

  g_return_if_fail (WB_IS_RENDERER (self));
  g_return_if_fail (!shader || WB_IS_SHADER (shader));
  g_return_if_fail (WB_IS_ENVIRONMENT (environment));
  g_return_if_fail (WB_IS_CAMERA (camera));

  if (!WB_RENDERER_GET_CLASS (self)->render_node)
    return;

  if (WB_RENDERER_GET_CLASS (self)->render_setup)
    WB_RENDERER_GET_CLASS (self)->render_setup (self, shader, environment, camera);

  data.self = self;
  data.environment = environment;
  data.shader = shader;
  data.camera = camera;

  g_queue_foreach (&priv->nodes, (GFunc)render_foreach_func, &data);
}

static void
update_foreach_func (WbRenderNode *node,
                     gpointer      user_data)
{
  UpdateForeachData * data = user_data;

  g_assert (WB_IS_RENDERER (data->self));
  g_assert (WB_IS_RENDER_NODE (node));

  WB_RENDERER_GET_CLASS (data->self)->update_node (data->self,
                                                   node,
                                                   data->time_step);
}

void
wb_renderer_update_all (WbRenderer *self,
                        guint       time_step)
{
  WbRendererPrivate *priv = wb_renderer_get_instance_private (self);
  UpdateForeachData data;

  if (!WB_RENDERER_GET_CLASS (self)->render_node)
    return;

  data.self = self;
  data.time_step = time_step;

  g_queue_foreach (&priv->nodes, (GFunc)update_foreach_func, &data);
}

