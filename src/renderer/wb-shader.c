/* wb-shader.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbShader"

#include "config.h"

#include <glib/gi18n.h>

#include "wb-shader.h"
#include "wb-macros.h"

typedef struct
{
  guint ID_;

  /* We will use the name property to store shaders in a hash table */
  gchar * name;

  GBytes * geometry_source;
  GBytes * pixel_source;
  GBytes * vertex_source;
} WbShaderPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (WbShader, wb_shader, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GBytes *empty_bytes;

static guint
create_shader (gint     type,
               GBytes  *src,
               GError **error)
{
  const gchar *shader_version;
  guint shader;
  int status;

  g_assert (error == NULL || *error == NULL);

  if (g_bytes_equal (src, empty_bytes))
    return 0;

  shader_version = g_strdup_printf ("#version %d core\n", GLSL_VERSION);

  shader = glCreateShader (type);

  glShaderSource (shader, 2,
                        (const char *[]) {
                          shader_version,
                          g_bytes_get_data (src, NULL),
                        },
                        (int[]) {
                          strlen (shader_version),
                          g_bytes_get_size (src),
                        });

  glCompileShader (shader);

  glGetShaderiv (shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE)
    {
      gint log_len;
      gchar *buffer;
      const gchar *type_str;

      glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &log_len);

      buffer = g_malloc (log_len + 1);
      glGetShaderInfoLog (shader, log_len, NULL, buffer);

      switch (type)
        {
        case GL_FRAGMENT_SHADER:
          type_str = "pixel";
          break;

        case GL_GEOMETRY_SHADER:
          type_str = "geometry";
          break;

        case GL_VERTEX_SHADER:
          type_str = "vertex";
          break;

        default:
          type_str = "unKnown";
          break;
        }

      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Compile failure in %s shader:\n%s",
                   type_str,
                   buffer);

      g_free (buffer);

      glDeleteShader (shader);

      return 0;
    }

  return shader;
}

static gboolean
wb_shader_compile (WbShader  *self,
                   GError   **error)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);
  guint geometry = 0;
  guint pixel = 0;
  guint vertex = 0;
  gint status;

  g_assert (WB_IS_SHADER (self));

  if (*error != NULL)
    return FALSE;

  vertex = create_shader (GL_VERTEX_SHADER, priv->vertex_source, error);
  if (vertex == 0)
    return FALSE;

  pixel = create_shader (GL_FRAGMENT_SHADER, priv->pixel_source, error);
  if (pixel == 0)
    {
      glDeleteShader (vertex);
      return FALSE;
    }

  geometry = create_shader (GL_GEOMETRY_SHADER, priv->geometry_source, error);

  priv->ID_ = glCreateProgram ();
  glAttachShader (priv->ID_, vertex);
  glAttachShader (priv->ID_, pixel);

  if (geometry != 0)
    glAttachShader (priv->ID_, geometry);

  glLinkProgram (priv->ID_);

  glGetProgramiv (priv->ID_, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
    {
      g_autofree gchar *buffer;
      gint log_len;

      glGetProgramiv (priv->ID_, GL_INFO_LOG_LENGTH, &log_len);

      buffer = g_malloc (log_len + 1);
      glGetProgramInfoLog (priv->ID_, log_len, NULL, buffer);

      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Shader program linking failure:\n%s", buffer);
      glDeleteProgram (priv->ID_);
      priv->ID_ = 0;
      return FALSE;
    }

  glDeleteShader (vertex);
  glDeleteShader (pixel);

  if (geometry != 0)
    glDeleteShader (geometry);

  return TRUE;
}

static gint
get_uniform_location (WbShader *self,
                      const gchar    *name)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);
  gint loc;

  g_assert (WB_IS_SHADER (self));
  g_assert (!wb_str_empty (name));

  if (priv->ID_ == 0)
    {
      g_critical ("Failed to lookup the uniform: Shader is either invalid or not compiled successfully.");
      return -1;
    }

  loc = glGetUniformLocation (priv->ID_, name);
  if (loc < 0)
    return -1;

  return loc;
}

static void
wb_shader_finalize (GObject *object)
{
  WbShader *self = (WbShader *)object;
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);

  glDeleteProgram (priv->ID_);

  g_clear_pointer (&priv->geometry_source, g_bytes_unref);
  g_clear_pointer (&priv->pixel_source, g_bytes_unref);
  g_clear_pointer (&priv->vertex_source, g_bytes_unref);

  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (wb_shader_parent_class)->finalize (object);
}

static void
wb_shader_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  WbShader *self = WB_SHADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, wb_shader_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_shader_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  WbShader *self = WB_SHADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      wb_shader_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_shader_class_init (WbShaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_shader_finalize;
  object_class->get_property = wb_shader_get_property;
  object_class->set_property = wb_shader_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the shader.",
                         "dummy",
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  empty_bytes = g_bytes_new (NULL, 0);
}

static void
wb_shader_init (WbShader *self)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);

  priv->name = g_strdup (_("dummy"));

  priv->geometry_source = g_bytes_ref (empty_bytes);
  priv->pixel_source = g_bytes_ref (empty_bytes);
  priv->vertex_source = g_bytes_ref (empty_bytes);
}


void
wb_shader_set_uniforms (WbShader *self)
{
  if (WB_SHADER_GET_CLASS (self)->set_uniforms)
    WB_SHADER_GET_CLASS (self)->set_uniforms (self);
}


void
wb_shader_bind (WbShader * self)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);
  g_return_if_fail (WB_IS_SHADER (self));

  glUseProgram (priv->ID_);
}

void
wb_shader_unbind (WbShader *self)
{
  g_return_if_fail (WB_IS_SHADER (self));
  glUseProgram (0);
}

void
wb_shader_set_ubool (WbShader       *self,
                     const gchar    *u_name,
                     const gboolean  u_value)
{
  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));

  glUniform1i (get_uniform_location (self, u_name),
                     u_value);
}

void
wb_shader_set_ufloat (WbShader     *self,
                      const gchar  *u_name,
                      const gfloat  u_value)
{
  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));

  glUniform1f (get_uniform_location (self, u_name),
                     u_value);
}

void
wb_shader_set_uint (WbShader    *self,
                    const gchar *u_name,
                    const gint   u_value)
{
  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));

  glUniform1i (get_uniform_location (self, u_name),
                     u_value);
}

void
wb_shader_set_umatrix (WbShader          *self,
                       const gchar       *u_name,
                       graphene_matrix_t *u_value)
{
  gfloat value [16];

  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));
  g_return_if_fail (u_value);

  graphene_matrix_to_float (u_value, value);
  glUniformMatrix4fv (get_uniform_location (self, u_name),
                            1, GL_FALSE, value);
}

void
wb_shader_set_uvector (WbShader        *self,
                       const gchar     *u_name,
                       graphene_vec3_t *u_value)
{
  gfloat value [3];

  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));
  g_return_if_fail (u_value);

  graphene_vec3_to_float (u_value, value);
  glUniform3fv (get_uniform_location (self, u_name),
                      1, value);
}

void
wb_shader_set_urgba (WbShader      *self,
                     const gchar   *u_name,
                     const GdkRGBA *u_value)
{
  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));
  g_return_if_fail (u_value);

  glUniform4f (get_uniform_location (self, u_name),
                     u_value->red,
                     u_value->green,
                     u_value->blue,
                     u_value->alpha);
}

void
wb_shader_set_upoint (WbShader           *self,
                      const gchar        *u_name,
                      graphene_point3d_t *u_value)
{
  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));
  g_return_if_fail (u_value);

  glUniform3f (get_uniform_location (self, u_name),
                     u_value->x,
                     u_value->y,
                     u_value->z);
}

void
wb_shader_set_samplers (WbShader    *self,
                        const gchar *u_name,
                        const guint  count)
{
  gint samplers [count];
  gint loc;

  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (u_name));

  loc = get_uniform_location (self, u_name);

  for (guint i = 0; i < count; i++)
    samplers [i] = i;

  glUniform1iv (loc, count, samplers);
}


static void
set_source (WbShader      *self,
            WbShaderType   kind,
            GBytes        *source_bytes,
            GError       **error)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);
  GBytes **loc = NULL;

  g_assert (WB_IS_SHADER (self));
  g_assert (kind == WB_SHADER_ALL ||
            kind == WB_VERTEX_SHADER ||
            kind == WB_PIXEL_SHADER);
  g_assert (error == NULL || *error == NULL);

  if (source_bytes == NULL)
    source_bytes = empty_bytes;

  /* If kind is ALL, then we need to split the fragment and
   * vertex shaders from the bytes and assign them individually.
   * This safely scans for FRAGMENT_SHADER and VERTEX_SHADER as
   * specified within the GLSL resources. Some care is taken to
   * use GBytes which reference the original bytes instead of
   * copying them.
   */
  if (kind == WB_SHADER_ALL)
    {
      gsize len = 0;
      const char *source;
      const char *vertex_shader_start;
      const char *fragment_shader_start;
      const char *endpos;
      GBytes *pixel_bytes;
      GBytes *vertex_bytes;

      g_clear_pointer (&priv->pixel_source, g_bytes_unref);
      g_clear_pointer (&priv->vertex_source, g_bytes_unref);

      source = g_bytes_get_data (source_bytes, &len);
      endpos = source + len;
      vertex_shader_start = g_strstr_len (source, len, "VERTEX_SHADER");
      fragment_shader_start = g_strstr_len (source, len, "PIXEL_SHADER");

      if (vertex_shader_start == NULL)
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "Failed to locate VERTEX_SHADER in shader source");
          return;
        }

      if (fragment_shader_start == NULL)
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "Failed to locate PIXEL_SHADER in shader source");
          return;
        }

      if (vertex_shader_start > fragment_shader_start)
        {

          g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                       "VERTEX_SHADER must come before PIXEL_SHADER");
          return;
        }

      /* Locate next newlines */
      while (vertex_shader_start < endpos && vertex_shader_start[0] != '\n')
        vertex_shader_start++;
      while (fragment_shader_start < endpos && fragment_shader_start[0] != '\n')
        fragment_shader_start++;

      vertex_bytes = g_bytes_new_from_bytes (source_bytes,
                                             vertex_shader_start - source,
                                             fragment_shader_start - vertex_shader_start);
      pixel_bytes = g_bytes_new_from_bytes (source_bytes,
                                               fragment_shader_start - source,
                                               endpos - fragment_shader_start);

      set_source (self, WB_VERTEX_SHADER, vertex_bytes, error);
      set_source (self, WB_PIXEL_SHADER, pixel_bytes, error);

      g_bytes_unref (pixel_bytes);
      g_bytes_unref (vertex_bytes);

      return;
    }

  if (kind == WB_PIXEL_SHADER)
    loc = &priv->pixel_source;
  else if (kind == WB_VERTEX_SHADER)
    loc = &priv->vertex_source;
  else
    g_return_if_reached ();

  if (*loc != source_bytes)
    {
      g_clear_pointer (loc, g_bytes_unref);
      *loc = g_bytes_ref (source_bytes);
    }
}

gboolean
wb_shader_set_source (WbShader     *self,
                      GBytes       *src,
                      GError      **error)
{
  g_return_val_if_fail (WB_IS_SHADER (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  set_source (self, WB_SHADER_ALL, src, error);

  return wb_shader_compile (self, error);
}

gchar *
wb_shader_get_name (WbShader *self)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);
  g_return_val_if_fail (WB_IS_SHADER (self), NULL);
  return priv->name;
}

void
wb_shader_set_name (WbShader    *self,
                    const gchar *name)
{
  WbShaderPrivate *priv = wb_shader_get_instance_private (self);

  g_return_if_fail (WB_IS_SHADER (self));
  g_return_if_fail (!wb_str_empty (name));

  if (!wb_str_equal (priv->name, name))
    {
      g_clear_pointer (&priv->name, g_free);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

