/* wb-light.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>
#include <graphene-gobject.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_LIGHT (wb_light_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (WbLight, wb_light, WB, LIGHT, GObject)

struct _WbLightClass
{
  GObjectClass parent_class;
};

WB_AVAILABLE_IN_ALL
graphene_point3d_t * wb_light_get_position          (WbLight *            self);
WB_AVAILABLE_IN_ALL
void                 wb_light_set_position          (WbLight *            self,
                                                     graphene_point3d_t * setting);

WB_AVAILABLE_IN_ALL
GdkRGBA *            wb_light_get_color             (WbLight *            self);
WB_AVAILABLE_IN_ALL
void                 wb_light_set_color             (WbLight *            self,
                                                     const GdkRGBA *      setting);

G_END_DECLS
