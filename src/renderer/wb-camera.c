/* wb-camera.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbCamera"

#include "config.h"

#include <epoxy/gl.h>

#include "wb-camera.h"

#define WB_CAMERA_FOV 45.0f
#define WB_CAMERA_NEAR_CLIP 0.1f
#define WB_CAMERA_FAR_CLIP 100000.0f

struct _WbCamera
{
  GObject parent_instance;

  graphene_point3d_t position;

  /* horizontal rotation */
  gfloat pitch;
  /* vertical rotation */
  gfloat yaw;

  gfloat roll;
  gfloat zoom;

  graphene_matrix_t * camera_matrix;
  graphene_matrix_t * projection_matrix;

  /* Helpers */
  graphene_quaternion_t * rotation;

  /* 2D/3D Camera */
  guint is_orthographic : 1;
};

G_DEFINE_FINAL_TYPE (WbCamera, wb_camera, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_MATRIX,
  PROP_POSITION,
  PROP_YAW,
  PROP_PITCH,
  PROP_ROLL,
  PROP_ZOOM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
wb_camera_finalize (GObject *object)
{
  WbCamera *self = (WbCamera *)object;

  g_clear_pointer (&self->camera_matrix, graphene_matrix_free);
  g_clear_pointer (&self->projection_matrix, graphene_matrix_free);

  g_clear_pointer (&self->rotation, graphene_quaternion_free);

  G_OBJECT_CLASS (wb_camera_parent_class)->finalize (object);
}

static void
wb_camera_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  WbCamera *self = WB_CAMERA (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_camera_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  WbCamera *self = WB_CAMERA (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_camera_class_init (WbCameraClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_camera_finalize;
  object_class->get_property = wb_camera_get_property;
  object_class->set_property = wb_camera_set_property;
}

static void
wb_camera_init (WbCamera *self)
{
  self->camera_matrix = graphene_matrix_alloc ();
  graphene_matrix_init_identity (self->camera_matrix);

  self->projection_matrix = graphene_matrix_alloc ();
  graphene_matrix_init_identity (self->projection_matrix);

  self->rotation = graphene_quaternion_alloc ();

  self->position = (graphene_point3d_t){ 0.0f, 0.0f, -1000.0f };
  self->yaw = 0.0f;
  self->pitch = 0.0f;
  self->roll = 0.0f;
  self->zoom = 1.0f;

  self->is_orthographic = !! FALSE;
}


WbCamera *
wb_camera_new (void)
{
  return g_object_new (WB_TYPE_CAMERA, NULL);
}

void
wb_camera_move (WbCamera     *self,
                const gfloat  delta_x,
                const gfloat  delta_y,
                const gfloat  delta_z)
{
  g_return_if_fail (WB_IS_CAMERA (self));

  self->position.x += delta_x;
  self->position.y += delta_y;
  self->position.z += delta_z;
}

void
wb_camera_rotate (WbCamera     *self,
                  const gfloat  delta_updown,
                  const gfloat  delta_leftright)
{
  g_return_if_fail (WB_IS_CAMERA (self));

  self->yaw += delta_updown;
  self->pitch += delta_leftright;
}

void
wb_camera_roll (WbCamera     *self,
                const gfloat  delta)
{
  g_return_if_fail (WB_IS_CAMERA (self));

  self->roll += delta;
}

void
wb_camera_zoom (WbCamera     *self,
                const gfloat  factor)
{
  g_return_if_fail (WB_IS_CAMERA (self));

  self->zoom += factor;

  if (self->zoom < 1.0f)
    self->zoom = 1.0f;
}

void
wb_camera_update_matrix (WbCamera *self)
{
  g_return_if_fail (WB_IS_CAMERA (self));

  graphene_quaternion_init_from_angles (self->rotation,
                                        self->yaw,
                                        self->pitch,
                                        self->roll);

  graphene_matrix_init_identity (self->camera_matrix);
  graphene_matrix_translate (self->camera_matrix, &self->position);
  graphene_matrix_rotate_quaternion (self->camera_matrix, self->rotation);
  graphene_matrix_scale (self->camera_matrix, self->zoom, self->zoom, self->zoom);
}

graphene_matrix_t *
wb_camera_get_matrix (WbCamera *self)
{
  g_return_val_if_fail (WB_IS_CAMERA (self), NULL);
  return self->camera_matrix;
}

void
wb_camera_set_viewport (WbCamera     *self,
                        GdkRectangle *viewport)
{
  g_return_if_fail (WB_IS_CAMERA (self));
  g_return_if_fail (viewport);

  if (self->is_orthographic)
    graphene_matrix_init_ortho (self->projection_matrix,
                                - (gfloat)viewport->width / 2, (gfloat)viewport->width / 2,
                                - (gfloat)viewport->height / 2, (gfloat)viewport->height / 2,
                                - 1.0f, 1.0f);
  else
    graphene_matrix_init_perspective (self->projection_matrix,
                                      WB_CAMERA_FOV,
                                      (gfloat)viewport->width / viewport->height,
                                      WB_CAMERA_NEAR_CLIP, WB_CAMERA_FAR_CLIP);
}

graphene_matrix_t *
wb_camera_get_projection (WbCamera *self)
{
  g_return_val_if_fail (WB_IS_CAMERA (self), NULL);
  return self->projection_matrix;
}
