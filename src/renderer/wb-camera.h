/* wb-camera.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>
#include <graphene-gobject.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_CAMERA (wb_camera_get_type())

WB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (WbCamera, wb_camera, WB, CAMERA, GObject)

WB_AVAILABLE_IN_ALL
WbCamera *          wb_camera_new            (void) G_GNUC_WARN_UNUSED_RESULT;

WB_AVAILABLE_IN_ALL
void                wb_camera_move           (WbCamera *     self,
                                              const gfloat   delta_x,
                                              const gfloat   delta_y,
                                              const gfloat   delta_z);
WB_AVAILABLE_IN_ALL
void                wb_camera_rotate         (WbCamera *     self,
                                              const gfloat   delta_updown,
                                              const gfloat   delta_leftright);
WB_AVAILABLE_IN_ALL
void                wb_camera_roll           (WbCamera *     self,
                                              const gfloat   delta);
WB_AVAILABLE_IN_ALL
void                wb_camera_zoom           (WbCamera *     self,
                                              const gfloat   factor);

WB_AVAILABLE_IN_ALL
void                wb_camera_update_matrix  (WbCamera *     self);
WB_AVAILABLE_IN_ALL
graphene_matrix_t * wb_camera_get_matrix     (WbCamera *     self);

WB_AVAILABLE_IN_ALL
void                wb_camera_set_viewport   (WbCamera *     self,
                                              GdkRectangle * viewport);
WB_AVAILABLE_IN_ALL
graphene_matrix_t * wb_camera_get_projection (WbCamera *     self);



G_END_DECLS
