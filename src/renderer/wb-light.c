/* wb-light.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbLight"

#include "config.h"

#include "wb-light.h"

typedef struct
{
  graphene_point3d_t position;
  GdkRGBA            color;
} WbLightPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (WbLight, wb_light, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_POSITION,
  PROP_COLOR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
wb_light_finalize (GObject *object)
{
  WbLight *self = (WbLight *)object;
  WbLightPrivate *priv = wb_light_get_instance_private (self);

  G_OBJECT_CLASS (wb_light_parent_class)->finalize (object);
}

static void
wb_light_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  WbLight *self = WB_LIGHT (object);

  switch (prop_id)
    {
    case PROP_POSITION:
      g_value_set_boxed (value, wb_light_get_position (self));
      break;

    case PROP_COLOR:
      g_value_set_boxed (value, wb_light_get_color (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_light_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  WbLight *self = WB_LIGHT (object);

  switch (prop_id)
    {
    case PROP_POSITION:
      wb_light_set_position (self, g_value_get_boxed (value));
      break;

    case PROP_COLOR:
      wb_light_set_color (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
wb_light_class_init (WbLightClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = wb_light_finalize;
  object_class->get_property = wb_light_get_property;
  object_class->set_property = wb_light_set_property;

  properties [PROP_POSITION] =
    g_param_spec_boxed ("position",
                        "Position",
                        "The location of light.",
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  properties [PROP_COLOR] =
    g_param_spec_boxed ("color",
                        "Color",
                        "The color tint of the light.",
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
wb_light_init (WbLight *self)
{
  WbLightPrivate *priv = wb_light_get_instance_private (self);

  priv->position = (graphene_point3d_t){ 0.0f, 0.0f, 0.0f };
  priv->color = (GdkRGBA){ 0.0f, 0.0f, 0.0f, 1.0f };
}


graphene_point3d_t *
wb_light_get_position (WbLight *self)
{
  WbLightPrivate *priv = wb_light_get_instance_private (self);
  g_return_val_if_fail (WB_IS_LIGHT (self), NULL);
  return &priv->position;
}

void
wb_light_set_position (WbLight            *self,
                       graphene_point3d_t *setting)
{
  WbLightPrivate *priv = wb_light_get_instance_private (self);

  g_return_if_fail (WB_IS_LIGHT (self));
  g_return_if_fail (setting);

  if (!graphene_point3d_equal (&priv->position, setting))
    {
      graphene_point3d_init_from_point (&priv->position, setting);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_POSITION]);
    }
}

GdkRGBA *
wb_light_get_color (WbLight *self)
{
  WbLightPrivate *priv = wb_light_get_instance_private (self);
  g_return_val_if_fail (WB_IS_LIGHT (self), NULL);
  return &priv->color;
}

void
wb_light_set_color (WbLight       *self,
                    const GdkRGBA *setting)
{
  WbLightPrivate *priv = wb_light_get_instance_private (self);

  g_return_if_fail (WB_IS_LIGHT (self));
  g_return_if_fail (setting);

  if (!gdk_rgba_equal (&priv->position, setting))
    {
      priv->color.red = setting->red;
      priv->color.green = setting->green;
      priv->color.blue = setting->blue;
      priv->color.alpha = setting->alpha;

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLOR]);
    }
}

