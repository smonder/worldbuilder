/* wb-shader.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gdk/gdk.h>
#include <graphene-gobject.h>
#include <epoxy/gl.h>

#include "wb-version-macros.h"

G_BEGIN_DECLS

#define WB_TYPE_SHADER (wb_shader_get_type())
#define WB_IS_SHADER_TYPE(s_type) ((s_type) <= WB_VERTEX_SHADER)

WB_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (WbShader, wb_shader, WB, SHADER, GObject)

typedef enum {
  WB_GEOMETRY_SHADER,
  WB_PIXEL_SHADER,
  WB_VERTEX_SHADER,
  WB_SHADER_ALL,
} WbShaderType;

struct _WbShaderClass
{
  GObjectClass parent_class;

  void         (* set_uniforms) (WbShader * shader);
};


WB_AVAILABLE_IN_ALL
gchar *    wb_shader_get_name      (WbShader *           self);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_name      (WbShader *           self,
                                    const gchar *        name);

WB_AVAILABLE_IN_ALL
void       wb_shader_set_uniforms  (WbShader *           self);

WB_AVAILABLE_IN_ALL
gboolean   wb_shader_set_source    (WbShader *           self,
                                    GBytes *             src,
                                    GError **            error);

WB_AVAILABLE_IN_ALL
void       wb_shader_bind          (WbShader *           self);
WB_AVAILABLE_IN_ALL
void       wb_shader_unbind        (WbShader *           self);

WB_AVAILABLE_IN_ALL
void       wb_shader_set_ubool     (WbShader *           self,
                                    const gchar *        u_name,
                                    const gboolean       u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_ufloat    (WbShader *           self,
                                    const gchar *        u_name,
                                    const gfloat         u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_uint      (WbShader *           self,
                                    const gchar *        u_name,
                                    const gint           u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_umatrix   (WbShader *           self,
                                    const gchar *        u_name,
                                    graphene_matrix_t *  u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_uvector   (WbShader *           self,
                                    const gchar *        u_name,
                                    graphene_vec3_t *    u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_urgba     (WbShader *           self,
                                    const gchar *        u_name,
                                    const GdkRGBA *      u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_upoint    (WbShader *           self,
                                    const gchar *        u_name,
                                    graphene_point3d_t * u_value);
WB_AVAILABLE_IN_ALL
void       wb_shader_set_samplers  (WbShader *           self,
                                    const gchar *        u_name,
                                    const guint          count);

G_END_DECLS

