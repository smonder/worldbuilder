/* wb-generic-shader.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbGenericShader"

#include "config.h"

#include "wb-generic-shader.h"

struct _WbGenericShader
{
  WbShader parent_instance;
};

G_DEFINE_FINAL_TYPE (WbGenericShader, wb_generic_shader, WB_TYPE_SHADER)


WbShader *
wb_generic_shader_new (void)
{
  g_autoptr (WbShader) self = NULL;
  GBytes * src;
  GError * error = NULL;

  self = g_object_new (WB_TYPE_GENERIC_SHADER,
                       "name", "generic_shader",
                       NULL);

  src = g_resources_lookup_data ("/io/sam/worldbuilder-shaders/generic-shader.glsl",
                                 G_RESOURCE_LOOKUP_FLAGS_NONE, &error);

  if (!src)
    {
      g_critical ("Error creating environment shader. %s", error->message);
      return NULL;
    }

  if (!wb_shader_set_source (self, src, &error))
    {
      g_critical ("Error compiling environment shader. %s", error->message);
      return NULL;
    }

  return g_steal_pointer (&self);
}

static void
wb_generic_shader_class_init (WbGenericShaderClass *klass)
{
}

static void
wb_generic_shader_init (WbGenericShader *self)
{
}


