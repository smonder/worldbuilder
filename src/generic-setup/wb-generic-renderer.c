/* wb-generic-renderer.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbGenericRenderer"

#include "config.h"

#include "wb-generic-renderer.h"
#include "wb-generic-shader.h"

struct _WbGenericRenderer
{
  WbRenderer parent_instance;

  WbShader * shader;
};

G_DEFINE_FINAL_TYPE (WbGenericRenderer, wb_generic_renderer, WB_TYPE_RENDERER)

static void
wb_generic_renderer_render_node (WbRenderer    *renderer,
                                 WbRenderNode  *node,
                                 WbShader      *shader,
                                 WbEnvironment *environment,
                                 WbCamera      *camera)
{
  WbGenericRenderer * self = (WbGenericRenderer *)renderer;

  g_assert (WB_IS_GENERIC_RENDERER (self));

  wb_shader_set_umatrix (self->shader, "u_ModelMatrix",
                         wb_render_node_get_matrix (node));

  wb_shader_set_samplers (self->shader, "u_Textures", 8);

  wb_render_node_bind_textures (node);

  wb_render_node_bind (node);
  wb_render_node_bind_ibuffer (node);

  switch (wb_render_node_get_solids_mode (node))
    {
    case WB_DRAW_POINTS:
      epoxy_glPolygonMode (GL_FRONT_AND_BACK, GL_POINT);
      break;

    case WB_DRAW_LINES:
      epoxy_glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
      break;

    case WB_DRAW_SOLIDS:
      epoxy_glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
      break;

    case WB_DRAW_INVALID_MODE:
    default: g_assert_not_reached ();
    }

  epoxy_glDrawElements (GL_TRIANGLES,
                        wb_render_node_get_capacity (node),
                        GL_UNSIGNED_INT, NULL);

  wb_render_node_unbind_ibuffer (node);
  wb_render_node_unbind (node);

  wb_shader_unbind (self->shader);
}

static void
wb_generic_renderer_render_setup (WbRenderer    *renderer,
                                  WbShader      *shader,
                                  WbEnvironment *environment,
                                  WbCamera      *camera)
{
  WbGenericRenderer * self = (WbGenericRenderer *)renderer;

  g_assert (WB_IS_GENERIC_RENDERER (self));
  g_assert (WB_IS_ENVIRONMENT (environment));
  g_assert (WB_IS_CAMERA (camera));
  g_assert (shader == NULL);

  wb_shader_bind (self->shader);

  wb_shader_set_umatrix (self->shader, "u_ViewMatrix",
                         wb_camera_get_matrix (camera));
  wb_shader_set_umatrix (self->shader, "u_ProjectionMatrix",
                         wb_camera_get_projection (camera));

  wb_shader_set_urgba (self->shader, "u_HorizonColor",
                       wb_environment_get_horizon_color (environment));

  wb_shader_set_upoint (self->shader, "u_SunPosition",
                        wb_light_get_position (wb_environment_get_sun (environment)));
  wb_shader_set_urgba (self->shader, "u_SunColor",
                       wb_light_get_color (wb_environment_get_sun (environment)));
  wb_shader_set_ufloat (self->shader, "u_SunDiffuse",
                        wb_sun_get_diffuse ((WbSun *)wb_environment_get_sun (environment)));
}

static void
wb_generic_renderer_class_init (WbGenericRendererClass *klass)
{
  WbRendererClass * renderer_class = WB_RENDERER_CLASS (klass);

  renderer_class->render_node = wb_generic_renderer_render_node;
  renderer_class->render_setup = wb_generic_renderer_render_setup;
}

static void
wb_generic_renderer_init (WbGenericRenderer *self)
{
  self->shader = wb_generic_shader_new ();
}


WbRenderer *
wb_generic_renderer_new (void)
{
  return g_object_new (WB_TYPE_GENERIC_RENDERER, NULL);
}
