/* wb-loader-obj.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbLoaderObj"

#include "config.h"
#include "wb-loader-private.h"

#define MAX_DATA_SIZE 1000000000


static gboolean
_construct_buffers (WbLoader     *self)
{
  g_assert (WB_IS_LOADER (self));

  for (guint i = 0; i < self->indices_mixed->len; i++)
    {
      MixedIndex in = g_array_index (self->indices_mixed, MixedIndex, i);
      WbVertex new_vert;
      graphene_point3d_t normals;

      new_vert.position = (graphene_point3d_t)g_array_index (self->vertex_positions, graphene_point3d_t, in.position - 1);
      new_vert.texture_uv = (graphene_point_t)g_array_index (self->vertex_texture_coords, graphene_point_t, in.textcoord - 1);
      new_vert.texture_uv.y = 1 - new_vert.texture_uv.y;

      normals = (graphene_point3d_t)g_array_index (self->vertex_normals, graphene_point3d_t, in.normal - 1);
      new_vert.color = (GdkRGBA) { normals.x, normals.y, normals.z, 1.0f };
      new_vert.texture_id = 0.0f;

      g_array_append_val (self->vertices, new_vert);
      g_array_append_val (self->indices, i);
    }

  return TRUE;
}

static gboolean
_parse_obj_data (WbLoader     *self,
                 GBytes       *data)
{
  gchar * obj_data;
  gsize size = 0;
  gchar ** obj_file_lines;
  guint line_iter = 0;

  g_assert (WB_IS_LOADER (self));

  obj_data = (gchar *)g_bytes_get_data (data, &size);
  obj_file_lines = g_strsplit_set (obj_data, "\n", -1);

  do
    {
      gchar *line = obj_file_lines [line_iter];

      if (g_str_has_prefix (line, "o "))
        {
          self->object_name = g_strdup (line + 2);
        }
      else if (g_str_has_prefix (line, "v "))
        {
          graphene_point3d_t point;
          gfloat x, y, z;

          sscanf (line, "v %f %f %f", &x, &y, &z);
          point = (graphene_point3d_t){ x, y, z };

          g_array_append_val (self->vertex_positions, point);
        }
      else if (g_str_has_prefix (line, "vt "))
        {
          graphene_point_t texture;
          gfloat u, v;

          sscanf (line, "vt %f %f", &u, &v);
          texture = (graphene_point_t){ u, v };

          g_array_append_val (self->vertex_texture_coords, texture);
        }
      else if (g_str_has_prefix (line, "vn "))
        {
          graphene_point3d_t normals;
          gfloat u, v, w;

          sscanf (line, "vn %f %f %f", &u, &v, &w);
          normals = (graphene_point3d_t){ u, v, w };

          g_array_append_val (self->vertex_normals, normals);
        }
      else if (g_str_has_prefix (line, "f "))
        {
          MixedIndex m_index1;
          MixedIndex m_index2;
          MixedIndex m_index3;

          guint p1, t1, n1;
          guint p2, t2, n2;
          guint p3, t3, n3;

          sscanf (line, "f %d/%d/%d %d/%d/%d %d/%d/%d",
                  &p1, &t1, &n1, &p2, &t2, &n2, &p3, &t3, &n3);

          m_index1 = (MixedIndex){ p1, t1, n1 };
          m_index2 = (MixedIndex){ p2, t2, n2 };
          m_index3 = (MixedIndex){ p3, t3, n3 };

          g_array_append_val (self->indices_mixed, m_index1);
          g_array_append_val (self->indices_mixed, m_index2);
          g_array_append_val (self->indices_mixed, m_index3);
        }
      else
        g_debug ("unhandled line");

      line_iter ++;
    }
  while (obj_file_lines [line_iter] != NULL);

  if (self->indices_mixed->len == 0 ||
      self->vertex_normals->len == 0 ||
      self->vertex_texture_coords->len == 0 ||
      self->vertex_positions->len == 0)
    {
      g_critical ("Failed to parse OBJ file data.");
      return FALSE;
    }

  g_clear_pointer (&obj_file_lines, g_strfreev);

  return _construct_buffers (self);
}

WbRenderNode *
wb_loader_load_obj_model (WbLoader    *self,
                          const gchar *res_path)
{
  g_autoptr (WbRenderNode) node = NULL;
  GBytes * obj_file_data;
  GError *error = NULL;

  g_return_val_if_fail (WB_IS_LOADER (self), NULL);
  g_return_val_if_fail (!wb_str_empty (res_path), NULL);

  obj_file_data = g_resources_lookup_data (res_path,
                                           G_RESOURCE_LOOKUP_FLAGS_NONE,
                                           &error);
  if (!obj_file_data)
    {
      g_critical ("Error loading model from resources. %s", error->message);
      return NULL;
    }

  if (!_parse_obj_data (self, obj_file_data))
    return NULL;

  node = wb_render_node_new (self->object_name, self->indices->len);

  wb_render_node_bind (node);
  wb_render_node_set_buffer_data (node, WB_VERTEX_BUFFER, 0,
                                  self->vertices->len * sizeof (WbVertex),
                                  self->vertices->data);
  wb_render_node_set_buffer_data (node, WB_INDEX_BUFFER, 0,
                                  self->indices->len * sizeof (guint),
                                  self->indices->data);
  wb_render_node_unbind (node);

  g_bytes_unref (obj_file_data);
  return g_steal_pointer (&node);
}
