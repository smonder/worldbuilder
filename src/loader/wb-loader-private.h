/* wb-loader-private.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <graphene-gobject.h>

#include "wb-loader.h"
#include "wb-macros.h"
#include "wb-vertex.h"

G_BEGIN_DECLS

struct _WbLoader
{
  GObject parent_instance;

  gchar * object_name;

  GArray * vertex_positions;
  GArray * vertex_texture_coords;
  GArray * vertex_normals;
  GArray * indices_mixed;

  /* the final result of the process */
  GArray * vertices;
  GArray * indices;
};

typedef struct {
  guint position;
  guint textcoord;
  guint normal;
} MixedIndex;


void          _wb_loader_import_obj_construct_buffers_async   (WbLoader *          self,
                                                               GBytes *            data,
                                                               GCancellable *      cancellable,
                                                               GAsyncReadyCallback callback,
                                                               gpointer            user_data);
gboolean      _wb_loader_import_obj_construct_buffers_finish  (WbLoader *          self,
                                                               GAsyncResult *      result,
                                                               GError **           error);


G_END_DECLS
