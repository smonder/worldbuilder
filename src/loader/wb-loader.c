/* wb-loader.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "WbLoader"

#include "config.h"

#include "wb-loader-private.h"

G_DEFINE_FINAL_TYPE (WbLoader, wb_loader, G_TYPE_OBJECT)

static WbLoader *the_singleton = NULL;

static void
wb_loader_finalize (GObject *object)
{
  WbLoader *self = (WbLoader *)object;

  g_clear_pointer (&self->object_name, g_free);

  g_clear_pointer (&self->vertex_positions, g_array_unref);
  g_clear_pointer (&self->vertex_texture_coords, g_array_unref);
  g_clear_pointer (&self->vertex_normals, g_array_unref);
  g_clear_pointer (&self->indices_mixed, g_array_unref);
  g_clear_pointer (&self->vertices, g_array_unref);
  g_clear_pointer (&self->indices, g_array_unref);

  G_OBJECT_CLASS (wb_loader_parent_class)->finalize (object);
}

static GObject*
wb_loader_constructor (GType                  type,
                       guint                  n_construct_params,
                       GObjectConstructParam *construct_params)
{
  GObject *object;

  if (!the_singleton)
    {
      object =
        G_OBJECT_CLASS (wb_loader_parent_class)->constructor (type,
                                                              n_construct_params,
                                                              construct_params);
      the_singleton = WB_LOADER (object);
    }
  else
    object = g_object_ref (G_OBJECT (the_singleton));

  return object;
}

static void
wb_loader_class_init (WbLoaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructor = wb_loader_constructor;
  object_class->finalize = wb_loader_finalize;
}

static void
wb_loader_init (WbLoader *self)
{
  self->vertex_positions = g_array_new (FALSE, FALSE, sizeof (graphene_point3d_t));
  self->vertex_texture_coords = g_array_new (FALSE, FALSE, sizeof (graphene_point_t));
  self->vertex_normals = g_array_new (FALSE, FALSE, sizeof (graphene_point3d_t));
  self->indices_mixed = g_array_new (FALSE, FALSE, sizeof (MixedIndex));

  self->vertices = g_array_new (FALSE, FALSE, sizeof (WbVertex));
  self->indices = g_array_new (FALSE, FALSE, sizeof (guint));
}


WbLoader *
wb_loader_get_default (void)
{
  return g_object_new (WB_TYPE_LOADER, NULL);
}

