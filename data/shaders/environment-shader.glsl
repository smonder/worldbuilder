// environment-shader.glsl:

// VERTEX_SHADER
layout (location = 0) in vec3 a_Position;

out vec3 v_UVs;

uniform mat4 u_ViewMatrix;
uniform mat4 u_ProjectionMatrix;

void
main()
{
  v_UVs = a_Position;

  gl_Position = u_ProjectionMatrix * u_ViewMatrix * vec4 (a_Position, 1.0);
}

// PIXEL_SHADER
out vec4 FragColor;
in vec3 v_UVs;

uniform samplerCube u_Textures;
uniform vec4 u_HorizonColor;

const float lower_limit = 0.0f;
const float upper_limit = 3000.0f;

void
main()
{
  vec4 color = texture(u_Textures, v_UVs);
  float factor = (v_UVs.y - lower_limit) / upper_limit - lower_limit;
  factor = clamp (factor, 0.0f, 1.0f);

  FragColor = mix (u_HorizonColor, color, factor);
}

