// generic-shader.glsl:

// VERTEX_SHADER
layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec4 a_Color;
layout (location = 2) in vec2 a_UVs;
layout (location = 3) in float a_Texture;

uniform mat4 u_ModelMatrix;
uniform mat4 u_ViewMatrix;
uniform mat4 u_ProjectionMatrix;
uniform vec3 u_SunPosition;

out vec2 v_UVs;
out float v_Texture;
out vec3 v_SunRaysVec;
out vec3 v_SurfaceNormals;
out float visibility;

const float fog_density = 0.0035f;
const float fog_gradient = 5.0f;

void
main()
{
  vec4 worldPosition = u_ModelMatrix * vec4 (a_Position, 1.0);
  vec4 cameraSpacePosition = u_ViewMatrix * worldPosition;
  vec3 a_Normal = vec3 (a_Color.x, a_Color.y, a_Color.z);

  gl_Position = u_ProjectionMatrix * cameraSpacePosition;

  v_SurfaceNormals = (u_ModelMatrix * vec4 (a_Normal, 0.0f)).xyz;
  v_SunRaysVec = - (u_SunPosition - worldPosition.xyz);

  float distanceFromCamera = length (cameraSpacePosition.xyz);
  visibility = exp (-pow ((distanceFromCamera * fog_density), fog_gradient));
  visibility = clamp (visibility, 0.0f, 1.0f);

  v_Texture = a_Texture;
}

// PIXEL_SHADER
in vec2 v_UVs;
in float v_Texture;
in vec3 v_SunRaysVec;
in vec3 v_SurfaceNormals;
in float visibility;

out vec4 FragColor;

uniform sampler2D u_Textures[8];
uniform vec4 u_HorizonColor;
uniform vec4 u_SunColor;
uniform float u_SunDiffuse;

const vec4 solid_color = vec4 (0.9f, 0.9f, 0.9f, 1.0f);

void
main()
{
  int index = int(v_Texture);

  float sun_brightness = dot (normalize (-v_SunRaysVec), normalize (v_SurfaceNormals));
  sun_brightness = max (sun_brightness, 0.2f);

  vec4 sun_diffuse = u_SunDiffuse * sun_brightness * u_SunColor;
  sun_diffuse.w = 1.0f;

  if (index == 0)
    {
      /* Keep the color always opaque. */
      FragColor = solid_color * sun_diffuse;
    }
  else
    {
      vec4 tex_Color = texture (u_Textures [index], v_UVs);
      FragColor = tex_Color * sun_diffuse;
    }

  FragColor = mix (FragColor, u_HorizonColor, visibility);
}

