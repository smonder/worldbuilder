project('worldbuilder', 'c',
          version: '0.1.0',
    meson_version: '>= 0.59.0',
  default_options: [ 'warning_level=2', 'werror=false', 'c_std=gnu11', ],
)

ABI_MAJOR = '42'
ABI_MINOR = '0'
ABI_MICRO = '0'

i18n = import('i18n')
gnome = import('gnome')
cc = meson.get_compiler('c')

glsl_req_version = '440'
opengl_req_version = '4.5'
wb_tracing = get_option('tracing')

app_data_dir = join_paths(meson.global_source_root(), 'data')
textures_dir = join_paths(app_data_dir, 'textures')
shaders_dir = join_paths(app_data_dir, 'shaders')
models_dir = join_paths(app_data_dir, 'models')
icons_dir = join_paths(app_data_dir, 'icons')

config_h = configuration_data()
config_h.set_quoted('PACKAGE_NAME', 'WorldBuilder')
config_h.set_quoted('PACKAGE_VERSION', meson.project_version())
config_h.set_quoted('GETTEXT_PACKAGE', 'WorldBuilder')
config_h.set_quoted('LOCALEDIR', join_paths(get_option('prefix'), get_option('localedir')))
config_h.set10('ENABLE_TRACING', wb_tracing)

# Setup some macros to control API functionality
opengl_major = opengl_req_version.split('.')[0].to_int()
opengl_minor = opengl_req_version.split('.')[1].to_int()
config_h.set('GLSL_VERSION', glsl_req_version)
config_h.set('OPENGL_VERSION_MAJOR_REQUIRED', opengl_major)
config_h.set('OPENGL_VERSION_MINOR_REQUIRED', opengl_minor)
config_h.set('MAX_TEXTURE_SLOTS', '32'.to_int())

configure_file(output: 'config.h', configuration: config_h)


add_project_arguments(['-I' + meson.project_build_root()], language: 'c')

project_c_args = []
test_c_args = [
  '-Wcast-align',
  '-Wdeclaration-after-statement',
  '-Werror=address',
  '-Werror=array-bounds',
  '-Werror=empty-body',
  '-Werror=implicit',
  '-Werror=implicit-function-declaration',
  '-Werror=incompatible-pointer-types',
  '-Werror=init-self',
  '-Werror=int-conversion',
  '-Werror=int-to-pointer-cast',
  '-Werror=main',
  '-Werror=misleading-indentation',
  '-Werror=missing-braces',
  '-Werror=missing-include-dirs',
  '-Werror=nonnull',
  '-Werror=overflow',
  '-Werror=parenthesis',
  '-Werror=pointer-arith',
  '-Werror=pointer-to-int-cast',
  '-Werror=redundant-decls',
  '-Werror=return-type',
  '-Werror=sequence-point',
  '-Werror=shadow',
  '-Werror=strict-prototypes',
  '-Werror=trigraphs',
  '-Werror=undef',
  '-Werror=write-strings',
  '-Wformat-nonliteral',
  '-Wignored-qualifiers',
  '-Wimplicit-function-declaration',
  '-Wlogical-op',
  '-Wmissing-declarations',
  '-Wmissing-format-attribute',
  '-Wmissing-include-dirs',
  '-Wmissing-noreturn',
  '-Wnested-externs',
  '-Wno-cast-function-type',
  '-Wno-dangling-pointer',
  '-Wno-missing-field-initializers',
  '-Wno-sign-compare',
  '-Wno-unused-parameter',
  '-Wold-style-definition',
  '-Wpointer-arith',
  '-Wredundant-decls',
  '-Wstrict-prototypes',
  '-Wswitch-default',
  '-Wswitch-enum',
  '-Wundef',
  '-Wuninitialized',
  '-Wunused',
  '-fno-strict-aliasing',
  ['-Werror=format-security', '-Werror=format=2'],
]
if get_option('buildtype') != 'plain'
  test_c_args += '-fstack-protector-strong'
endif
foreach arg: test_c_args
  if cc.has_multi_arguments(arg)
    project_c_args += arg
  endif
endforeach
add_project_arguments(project_c_args, language: 'c')

subdir('data')
subdir('src')
subdir('po')

gnome.post_install(
     glib_compile_schemas: true,
    gtk_update_icon_cache: true,
  update_desktop_database: true,
)
