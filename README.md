WorldBuilder is an open-source GTK+ and OpenGL linux game to test native gaming
on Linux and C.

⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣀⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⡴⠞⠋⠉⠀⠀⠀⠉⠙⠓⢦⣀⠀⠀⣠⡴⠒⠚⠛⠛⠛⠒⠲⢤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⡟⠛⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢷⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣆⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⣰⡟⠁⠀⠀⠀⢀⣠⠤⠖⠚⠛⠛⠛⠲⠦⣄⡀⠹⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⡄⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣰⠏⠀⠀⠀⠀⠞⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠳⣿⡒⠛⠛⠉⠉⠉⠉⠉⠙⠛⠛⠿⢤⣄⡀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢠⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡤⠶⠖⢒⣒⣒⣒⠚⠻⢦⡀⠀⣀⣤⣤⠶⠶⠶⠶⣶⣶⣤⣿⣦⡀⠀⠀
⠀⠀⠀⠀⠀⣠⠴⣿⠀⠀⠀⠀⠀⠀⠀⢀⣠⠴⣞⣯⠷⠞⠋⠉⠉⠀⠈⠉⠉⠙⠳⣿⣿⠿⠛⠛⠉⠉⠉⣉⡉⠉⢙⡛⠻⠿⣧⠀
⠀⠀⠀⢠⠞⠁⢀⡇⠀⠀⠀⠀⠀⣴⡾⠿⠒⠛⠉⠀⠀⣀⣀⣤⣤⣶⣶⠚⠋⠉⠉⠙⣧⣤⣴⣶⣾⣿⡟⣻⡏⠉⠉⠉⠉⠙⠛⡇
⠀⠀⢠⠏⠀⠀⠈⠁⠀⠀⠀⠀⠀⠈⠛⠓⠶⣶⠒⠋⢻⣿⣿⣿⣿⣷⡿⠀⠀⠀⠀⠀⡼⠸⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⢀⡟
⠀⢠⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠰⣬⣙⡲⠮⣽⣿⣿⣿⣫⣤⣤⡤⢶⣒⣿⠗⠒⠦⠭⠭⢭⣭⣤⣤⠤⠴⠶⢶⡿⠟⠀
⠀⡞⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠛⠓⠶⠶⠶⠤⠶⢶⣶⠟⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⠏⠀⠀⠀
⠘⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡤⠶⠋⠁⠀⠀⠀⠈⠛⠲⢶⣤⠤⠤⠴⠶⢿⡉⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠚⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠀⠀⠀⠀⠀⠙⢦⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣧⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣤⣤⣤⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣼⣄⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡾⠋⠀⣤⣄⣀⣀⣉⡉⠉⠙⠛⠛⠛⠻⠶⠶⠶⠶⠦⠤⠤⠤⠤⠴⠶⠶⠒⠛⠉⠁⣠⡿⠀⠀
⠀⡀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡀⠘⢷⣄⠀⠀⠀⠀⠉⠉⠙⠛⠛⠛⠲⠶⠶⠶⠤⣤⣤⣤⣤⣤⣤⣤⣤⣤⣤⠤⠤⠴⣿⠉⠀⠀⠀
⠰⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠳⣄⠀⠉⠉⠉⠉⠉⠛⠛⠒⠶⠶⠦⣤⣤⣄⣀⣀⣀⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⡿⠀⠀⠀⠀
⠀⣹⣷⣤⣀⠀⠀⠀⠀⠀⠀⠀⠉⠙⠂⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠉⠉⠉⠉⠉⠉⢉⣹⠟⠋⠉⠁⠀⠀⠀⠀⠀
⠀⡏⠙⠻⢿⣿⣶⠦⣤⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡤⠖⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⡇⠀⠀⠀⠉⠛⠿⢶⣤⣌⣉⠉⠉⠛⠛⠓⠒⠶⠶⠤⠤⢤⣤⣤⣤⠤⠤⠤⠴⢶⣶⣾⡉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⡇⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠙⠛⠓⠲⠶⠶⠶⠶⠶⠶⠶⠶⠶⠶⠶⠚⠛⠛⠉⠁⠀⠙⠲⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢳⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠃⠀⠀⠀⠐⠛⠂⠀⠐⠒⠒⠛⠛⠛⠛⠛⠛⠛⠛⠓⠚⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀

## NOTES
1. **WorldBuilder** is written in **C**.
2. **WorldBuilder** by default is implementing **OpenGL** as graphics API.

## Dependencies

 * GTK 4.6 or newer
 * Libadwaita
 * libepoxy
 * graphene

Refer to the [io.sam.WorldBuilder.json](https://gnome.gitlab.org/smonder/worldbuilder/blob/master/io.sam.WorldBuilder.json) Flatpak manifest for additional details.

## Try it Out!

### Building

We use the Meson (and thereby Ninja) build system for Forger. The quickest
way to get going is to do the following:

```sh
meson . ./build
ninja -C ./build
ninja -C ./build install
```

For build options see [meson_options.txt](./meson_options.txt).
